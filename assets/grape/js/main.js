var uagent = navigator.userAgent.toLowerCase();
var is_safari = ((uagent.indexOf('safari') != -1) || (navigator.vendor == "Apple Computer, Inc."));
var is_ie = ((uagent.indexOf('msie') != -1) && (!is_opera) && (!is_safari) && (!is_webtv));
var is_ie4 = ((is_ie) && (uagent.indexOf("msie 4.") != -1));
var is_moz = (navigator.product == 'Gecko');
var is_ns = ((uagent.indexOf('compatible') == -1) && (uagent.indexOf('mozilla') != -1) && (!is_opera) && (!is_webtv) && (!is_safari));
var is_ns4 = ((is_ns) && (parseInt(navigator.appVersion) == 4));
var is_opera = (uagent.indexOf('opera') != -1);
var is_kon = (uagent.indexOf('konqueror') != -1);
var is_webtv = (uagent.indexOf('webtv') != -1);
var is_win = ((uagent.indexOf("win") != -1) || (uagent.indexOf("16bit") != -1));
var is_mac = ((uagent.indexOf("mac") != -1) || (navigator.vendor == "Apple Computer, Inc."));
var is_chrome = (uagent.match(/Chrome\/\w+\.\w+/i)); if(is_chrome == 'null' || !is_chrome || is_chrome == 0) is_chrome = '';
var ua_vers = parseInt(navigator.appVersion);
var req_href = location.href;
var scrollTopForFirefox = 0;
var url_next_id = 1;
var v_interval = false;
var Page = {
    Loading: function(f) {
        var top_pad = $(window).height() / 2 - 50;
        if (f == 'start') {
            $('#loading').remove();
            $('html, body').append('<div id="loading" style="margin-top:' + top_pad + 'px"><div class="loadstyle"></div></div>');
            $('#loading').show();
        }
        if (f == 'stop') {
            $('#loading').remove();
        }
    },
    Go: function(h) {
        hh = (location.href).replace('https://' + location.host + '/', '');
        history.pushState({
            link: h
        }, null, h);
        $('.js_titleRemove, .box').remove();
        clearInterval(v_interval);
        Page.Loading('start');
        $('#page').load(h, {
            ajax: 'yes'
        }, function(data) {
            Page.Loading('stop');
            $('html, body').scrollTop(0);
        }).css('min-height', '0px');
    },
    Prev: function(h) {
        clearInterval(v_interval);
        $('.box').remove();
        Page.Loading('start');
        $('#page').load(h, {
            ajax: 'yes'
        }, function(data) {
            Page.Loading('stop');
            $('html, body').scrollTop(0);
        }).css('min-height', '0px');
    }
}
function request(path, vars, as)
{   
    var path = path || document.location;
    var vars = vars || '';
    console.log(as);
    var response = NaN;
    $.ajax({
        type: 'POST',
        url:  path,
        data: vars,
        global: false,
        async: as,
        success: function(resp) {
            response = resp;
        },
        error:  function(xhr, str){
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    });
    return response;
}
function empty(mixed_var)
{
    return (
        mixed_var === "" || mixed_var === 0  || mixed_var === "0" || mixed_var === undefined ||
        mixed_var === null || mixed_var === false  ||  ( is_array(mixed_var) && mixed_var.length === 0 ) );
}
//LM BOX
var lmBox = {
    start: function(){
        Page.Loading('start');
    },
    stop: function(){
        Page.Loading('stop');
    },
    win: function(i, d, o, h){
        lmBox.stop();
        if(is_moz && !is_chrome) scrollTopForFirefox = $(window).scrollTop();
        $('html, body').css('overflow-y', 'hidden');
        if(is_moz && !is_chrome) $(window).scrollTop(scrollTopForFirefox);
        $('body').append('<div class="lm_box no_display" id="newbox_miniature'+i+'">'+d+'</div>');
        $('.lm_box').fadeIn(200);
        $(window).keydown(function(event){
            if(event.keyCode == 27) 
                lmBox.clos(i, o, h);
        });
    },
    clos: function(i, o, h){
        $('#newbox_miniature'+i).remove();
        // if(o) $('html, body').css('overflow-y', 'auto');
        $('html, body').css('overflow-y', 'auto');
        if(h) history.pushState({link:h}, null, h);
    }
}
//LANG
var trsn = {
  box: function(){
    $('.js_titleRemove').remove();
    lmBox.start();
    $.post('/register/', function(d){
      lmBox.win('lm_lang_box', d);
    });
  }
}
function jsonResponse(path, vars)
{
}
 
function call(path, number) {
    if (number === undefined) number = '';
    var msg = $('.ajax_form' + number).serialize();
    console.log(msg);
    $.ajax({
        type: 'POST',
        url: path,
        data: msg,
        success: function(resp) {
            var notices = jQuery.parseJSON(resp);
            // if (notices.notice != '') alert(notices.notice);
            if (notices.error != '') alert(notices.error);
            location.reload();
        },
        error:  function(xhr, str){
            alert('Возникла ошибка: ' + xhr.responseCode);
        }
    });
 }
function filter(name, value) {
    var get = window.location.search;
    if (get == '') sep = '?';
    else sep = '&';
    get += sep + 'filter[name]=' + name + '&filter[value]=' + value;
    window.location.replace(location.pathname + get);
 }