<?php  
/* 
	Appointment: Главная страница
	File: install.php
	Author: Phakof
	Engine: Livemon
	Copyright: Алексеев Семен (с) 2017
	E-mail: p@qd2.ru
	URL: https://livemon.ru/
	Данный код защищен авторскими правами
*/
session_start();

@error_reporting ( E_ALL ^ E_WARNING ^ E_NOTICE );
@ini_set ( 'display_errors', true );
@ini_set ( 'html_errors', false );
@ini_set ( 'error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE );

define('RD', dirname (__FILE__));
define('APP', RD.'/app');

$distr_charset = "utf-8";
$db_charset = "utf8";

header("Content-type: text/html; charset=".$distr_charset);

$skin_header = '
<!doctype html>
<html>
<head>
  <meta charset="'.$distr_charset.'">
  <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
  <meta content="IE=edge,chrome=1" https-equiv="X-UA-Compatible">
  <title>Livemon CMS - Установка</title>
  <link rel="shortcut icon" type="image/ico" href="/fav.png">
  <link href="assets/grape/css/application.css" media="screen" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="assets/grape/js/application.js"></script>
<style type="text/css">
body {
	background: #ececec;
}
</style>
</head>
<body>
<script language="javascript" type="text/javascript">
var dle_act_lang   = ["Да", "Нет", "Ввод", "Отмена", "Загрузка изображений и файлов на сервер"];
var cal_language   = {en:{months:[\'Январь\',\'Февраль\',\'Март\',\'Апрель\',\'Май\',\'Июнь\',\'Июль\',\'Август\',\'Сентябрь\',\'Октябрь\',\'Ноябрь\',\'Декабрь\'],dayOfWeek:["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"]}};
</script>
<nav class="navbar navbar-default navbar-inverse navbar-static-top" role="navigation">
  <div class="navbar-header">
    <a class="navbar-brand" href="">Мастер установки Livemon CMS</a>
  </div>
</nav>
<div class="container">
  <div class="col-md-8 col-md-offset-2">
    <div class="padded">
	    <div style="margin-top: 10px;">';
$skin_footer = '</div></div></div></div></body></html>';
function msgbox($type, $title, $text, $back=FALSE){
global $lang, $skin_header, $skin_footer;
  if ($back) $back = "onclick=\"history.go(-1); return false;\""; else $back = "";
echo $skin_header. '<form action="install.php" method="get">
<div class="box">
  <div class="box-header">
    <div class="title">'.$title.'</div>
  </div>
  <div class="box-content">
	<div class="row box-section">'.$text.'</div>
	<div class="row box-section">
		<input '.$back.' class="btn btn-green" type=submit value="Продолжить">
	</div>
  </div>
</div>
</form>'. $skin_footer;
  exit();
}
extract($_REQUEST, EXTR_SKIP);
if($_REQUEST['action'] == "eula")
{
  if ( !$_SESSION['lm_install'] ) msgbox( "", "Ошибка", "Установка скрипта была начата не с начала. Вернитесь на главную страницу начала установки скрипта: <br /><br /><a href=\"https://{$_SERVER['HTTP_HOST']}/install.php\">https://{$_SERVER['HTTP_HOST']}/install.php</a>" );
  
echo $skin_header. '<form id="check-eula" method="get" action="">
<input type="hidden" name="action" value="function_check">
<script language="javascript">
function check_eula(){
	if( document.getElementById( "eula" ).checked == true )
	{
		return true;
	}
	else
	{
		LMalert( "Вы должны принять лицензионное соглашение, прежде чем продолжите установку.", "Информация" );
		return false;
	}
};
document.getElementById( "check-eula" ).onsubmit = check_eula;
</script>
<div class="box">
  <div class="box-header">
  <h1 class="title">Лицензионное соглашение</h1>
  </div>
  <div class="box-content">
	<div class="row box-section">
    <div class="txt">
    Пожалуйста, внимательно прочитайте и примите пользовательское соглашение по использованию Livemon.
    </div>
    <div style="height: 300px; border: 1px solid #76774C; background-color: #FDFDD3; padding: 5px; overflow: auto;">
    <h2 style="text-align:center;">Пользовательское лицензионное соглашение на использование программы для ЭВМ "Livemon"</h2>
    <span style="color:#FF0000">Уважаемый Пользователь! Перед началом установки, копирования либо иного использования Программы внимательно ознакомьтесь с условиями ее использования, содержащимися в настоящем Соглашении. Данное соглашение расторгается автоматически, если Вы отказываетесь выполнять условия нашего соглашения. Данное сублицензионное соглашение может быть расторгнуто нами в одностороннем порядке, в случае установления фактов нарушения данного сублицензионного соглашения. В случае досрочного расторжения соглашения Вы обязуетесь удалить все Ваши копии нашей Программы в течении 3 рабочих дней, с момента получения соответствующего уведомления.</span>
        <h3>Используя данные скрипты, Вы тем самым соглашаетесь с настоящей лицензией:</h3>
    <ol class="list">
      <li>
    Условия распространения
        <ul class="list">
            <li>Livemon CMS (далее программа) предоставляется на безвозмездной основе (бесплатно).</li>
            <li>Программа предоставляется как есть, авторы не несут никакой ответственности за
               работоспособность и то, как и в каких целях будет использоваться программа</li>
            <li>Все официальные версии программы публикуются ИСКЛЮЧИТЕЛЬНО
               на сайте https://livemon.ru/</li>
            <li>Запрещается распространять модифицированные копии Livemon CMS.
               Если Вы пишите какой-либо модуль, или дополнение, он должен устанавливаться
               на стандартную версию Livemon CMS, скачанную с оф. сайта https://livemon.ru/</li>
        </ul>
      </li>
      <li>Условия использования
          <ul class="list">
              <li>Устанавливая систему, вы должны в течение 3-х месяцев с момента установки сохранить
             копирайт "Livemon CMS" со ссылкой на https://Livemon.ru/ внизу страниц сайта.</li>
              <li>По истечении 3-х месяцев со дня установки, Вы имеете право удалить ссылку с копирайтом со страниц
             Вашего сайта. Однако, мы (разработчики) будем Вам очень благодарны, если вы сохраните ссылки
             Тем самым Вы поддерживаете наш труд, за который мы не просим никаких денег.</li>
          </ul>
      </li>
     <li>Поддержка продукта
         <ul class="list">
              <li>Официальная поддержка пользователей оказывается на сайте https://Livemon.ru/</li>
              <li>Авторы не гарантируют работоспособность программы на всех хостингах</li>
              <li>В связи с занятостью, авторы не могут гарантировать рассмотрение и решение всех возникающих вопросов, связанных с установкой и эксплуатацией стемы, однако по мере возможности, будет оказываться необходимая поддержка.</li>
         </ul>
      </li>
      <li>Взимание платы за услуги
         <ul class="list">
             <li>Livemon CMS является бесплатной программой, и Вы не имеете право продавать ее копии.</li>
             <li>Вы имеете право оказывать платные услуги по установке и настройке программы.</li>
             <li>Вы имеете право создавать и продавать дополнительные модули для системы при условии:
                  <ul class="list">
                     <li>Автором модуля, или дизайна (или правообладателем) являетесь Вы</li>
                     <li>В оригинальных файлах исходной системы Livemon CMS должны быть сохранены все авторские копирайты</li>
                     <li>Если для работы Вашего модуля требуется модификация исходных кодов оригинальной системы, Вы должны в readme.txt файле указать, какие именно файлы подвергались модификации и в любом случае сохранить авторские копирайты.</li>
                  </ul>
              </li>
          </ul>
      </li>
      <li>Использование кода скриптов
          <ul class="list">
              <li>Вы можете как угодно дорабатывать исходный код скриптов, при условии, что он будет использоваться только на Вашем проекте и не поступит в свободный доступ. Обязательным условием является сохранение оригинального копирайта в META тэге.</li>
              <li>Вы имеете право использовать исходный код скриптов Livemon CMS в своих разработках, при условии, что Вы пишите модули, предназначенные для Livemon CMS</li>
              <li>Если Вы собираетесь использовать исходный код скриптов в собственных разработках, не связанных с Livemon CMS и эти разработки находятся в свободном доступе (или продаются), то обязательно предварительное согласование с авторами скриптов Livemon CMS. Данное ограничение не касается сторонних библиотек под лицензией GPL</li>
          </ul>
      </li>
    </ol>
    </div>
		<br /><br /><input type="checkbox" name="eula" id="eula" class="icheck"><label for="eula"> Я принимаю данное соглашение</label>
	</div>
	<div class="row box-section">
		<input class="btn btn-green" type="submit" value="Продолжить">
	</div>
  </div>
</div>
</form>';

}elseif($_REQUEST['action'] == "function_check") {

    if ( !$_SESSION['lm_install'] ) msgbox( "", "Ошибка", "Установка скрипта была начата не с начала. Вернитесь на главную страницу начала установки скрипта: <br /><br /><a href=\"https://".$_SERVER['HTTP_HOST']."/install.php\">https://".$_SERVER['HTTP_HOST']."/install.php</a>" );
  
  echo $skin_header.'<form method="get" action="">
<input type=hidden name="action" value="chmod_check">
<div class="box">
  <div class="box-header">
    <div class="title">Проверка установленных компонентов PHP</div>
  </div>
  <div class="box-content">
	<div class="row box-section">
<table class="table table-normal table-bordered">
<tr>
<td width="250">Минимальные требования скрипта</td>
<td colspan="2">Текущее значение</td>
</tr>
<tr>
<td>Версия PHP 5.3.7 и выше</td>
<td colspan=2>'.(( version_compare(phpversion(), '5.3.7', '<')) ? '<font color=red><b>Нет</b></font>' : '<font color=green><b>Да</b></font>').'</td>
</tr><tr>
<td>Поддержка MySQLi</td>
<td colspan=2>'.(function_exists('mysqli_connect') ? '<font color=green><b>Да</b></font>' : '<font color=red><b>Нет</b></font>').'</td>
</tr><tr>
<td>Поддержка сжатия ZLib</td>
<td colspan=2>'.(extension_loaded('zlib') ? '<font color=green><b>Да</b></font>' : '<font color=red><b>Нет</b></font>').'</td>
</tr><tr>
<td>Поддержка XML</td>
<td colspan=2>'.(extension_loaded('xml') ? '<font color=green><b>Да</b></font>' : '<font color=red><b>Нет</b></font>').'</td>
</tr><tr>
<td>Поддержка многобайтных строк</td>
<td colspan=2>'.(function_exists('mb_convert_encoding') ? '<font color=green><b>Да</b></font>' : '<font color=red><b>Нет</b></font>').'</td>
</tr><tr>
<td colspan=3><br />Если любой из этих пунктов выделен красным, то пожалуйста выполните действия для исправления положения. В случае несоблюдения минимальных требований скрипта возможна его некорректная работа в системе.<br /><br /></td>
</tr></table>
</div>
<div class="row box-section">
	<input class="btn btn-green" type=submit value="Продолжить">
</div>
</div>
</div></form>';
}
/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1*/
elseif($_REQUEST['action'] == "chmod_check")
{

if ( !$_SESSION['lm_install'] ) msgbox( "", "Ошибка", "Установка скрипта была начата не с начала. Вернитесь на главную страницу начала установки скрипта: <br /><br /><a href=\"https://{$_SERVER['HTTP_HOST']}/install.php\">https://{$_SERVER['HTTP_HOST']}/install.php</a>" );

  echo $skin_header. '<form method="get" action="">
<input type="hidden" name="action" value="doconfig">
<div class="box">
  <div class="box-header">
    <div class="title">Проверка на запись у важных файлов системы</div>
  </div>
  <div class="box-content">
	<div class="row box-section">
<table class="table table-normal table-bordered"><thead><tr>
<td>Папка/Файл</td>
<td width="100">CHMOD</td>
<td width="100">Статус</td></tr></thead><tbody>';

$important_files = array(
'./modules/',
'./uploads/',
'./assets/',
'./app/',
'./app/cache',
);
$chmod_errors = 0;
$not_found_errors = 0;
    foreach($important_files as $file){
        if(!file_exists($file)){
            $file_status = "<font color=red>не найден!</font>";
            $not_found_errors ++;
        }
        elseif(is_writable($file)){
            $file_status = "<font color=green>разрешено</font>";
        }
        else{
            @chmod($file, 0777);
            if(is_writable($file)){
                $file_status = "<font color=green>разрешено</font>";
            }else{
                @chmod("$file", 0755);
                if(is_writable($file)){
                    $file_status = "<font color=green>разрешено</font>";
                }else{
                    $file_status = "<font color=red>запрещено</font>";
                    $chmod_errors ++;
                }
            }
        }
        $chmod_value = @decoct(@fileperms($file)) % 1000;

echo"<tr><td>".$file."</td><td>".$chmod_value."</td><td>".$file_status."</td></tr>";
    }
    
if($chmod_errors == 0 and $not_found_errors == 0){
    $status_report = 'Проверка успешно завершена! Можете продолжить установку!';
} else {
    if($chmod_errors > 0){
        $status_report = "<font color=red>Внимание!!!</font><br /><br />Во время проверки обнаружены ошибки: <b>$chmod_errors</b>. Запрещена запись в файл.<br />Вы должны выставить для папок CHMOD 777, для файлов CHMOD 666, используя FTP клиент.<br /><br /><font color=red><b>Настоятельно не рекомендуется</b></font> продолжать установку, пока не будут произведены изменения.<br />";
    }
    if($not_found_errors > 0){
        $status_report .= "<font color=red>Внимание!!!</font><br />Во время проверки обнаружены ошибки: <b>$not_found_errors</b>. Файлы не найдены!<br /><br /><font color=red><b>Не рекомендуется</b></font> продолжать установку, пока не будут произведены изменения.<br />";
    }
}
echo'<tr><td height="25" colspan=3>&nbsp;&nbsp;Состояние проверки</td></tr><tr><td style="padding: 5px" colspan=3>'.$status_report.'</td></tr></tbody></table>
	</div>
	<div class="row box-sectio">
		<input class="btn btn-green" type="submit" value="Продолжить">
	</div>
  </div>
</div></form>';

} elseif($_REQUEST['action'] == "doconfig") {

    if ( !$_SESSION['lm_install'] ) msgbox( "", "Ошибка", "Установка скрипта была начата не с начала. Вернитесь на главную страницу начала установки скрипта: <br /><br /><a href=\"https://{$_SERVER['HTTP_HOST']}/install.php\">https://{$_SERVER['HTTP_HOST']}/install.php</a>" );
    $url  = preg_replace( "'/install.php'", "", $_SERVER['HTTP_REFERER']);
    $url  = preg_replace( "'\?(.*)'", "", $url);
    if(substr("$url", -1) == "/"){
        $url = substr($url, 0, -1);
    }
  echo $skin_header.'<form method="post" action="">
<input type=hidden name="action" value="doinstall">
<div class="box">
  <div class="box-header">
    <div class="title">Настройка конфигурации системы</div>
  </div>
  <div class="box-content">
	<div class="row box-section">
<table width="100%"><tr><td width="175" style="padding: 5px;">URL сайта:
<td style="padding: 5px;"><input name=url value="'.$url.'/" size=38 type=text><br><span class="note large">Укажите путь без имени файла, знак слеша <font color="red">/</font> на конце обязателен</span></tr>
<tr><td colspan="3" height="40">&nbsp;&nbsp;<b>Данные для доступа к MySQL серверу</b><td></tr>
<tr><td style="padding: 5px;">Сервер MySQL:<td style="padding: 5px;"><input type=text size="28" name="dbhost" value="localhost"></tr>
<tr><td style="padding: 5px;">Имя базы данных:<td style="padding: 5px;"><input type=text size="28" name="dbname"></tr>
<tr><td style="padding: 5px;">Имя пользователя:<td style="padding: 5px;"><input type=text size="28" name="dbuser"></tr>
<tr><td style="padding: 5px;">Пароль:<td style="padding: 5px;"><input type=text size="28" name="dbpasswd"></tr>
<tr><td style="padding: 5px;">Префикс:<td style="padding: 5px;"><input type=text size="28" name="dbprefix" value="lm"> <span class="note large">Не изменяйте параметр, если не знаете для чего он предназначен</span></tr>
<tr><td colspan="3"  height="40">&nbsp;&nbsp;<b>Данные для доступа к панели управления</b><td></tr>
<tr><td style="padding: 5px;">Имя администратора:<td style="padding: 5px;"><input type=text size="28" name="reg_username" ></tr>
<tr><td style="padding: 5px;">Пароль:<td style="padding: 5px;"><input type=password size="28" name="reg_password1"> <span class="note large"><b>не</b> забудьте пароль!</span></tr>
<tr><td style="padding: 5px;">Повторите пароль:<td style="padding: 5px;"><input type=password size="28" name="reg_password2"></tr>
<tr><td style="padding: 5px;">E-mail:<td style="padding: 5px;"><input type=text size="28" name="reg_email"></tr></table>
	</div>
	<div class="row box-section">
		<input class="btn btn-green" type=submit value="Продолжить">
	</div>
  </div>
</div></form>';

}
// Do Install
elseif($_REQUEST['action'] == "doinstall")
{

	if ( !$_SESSION['lm_install'] ) msgbox( "", "Ошибка", "Установка скрипта была начата не с начала. Вернитесь на главную страницу начала установки скрипта: <br /><br /><a href=\"https://{$_SERVER['HTTP_HOST']}/install.php\">https://{$_SERVER['HTTP_HOST']}/install.php</a>" );

    if(!$reg_username or !$reg_email or !$reg_password1 or !$url or $reg_password1 != $reg_password2){ msgbox("error", "Ошибка!!!" ,"Заполните необходимые поля!", "history.go(-1)"); }
	
	if (preg_match("/[\||\'|\<|\>|\[|\]|\"|\!|\?|\$|\@|\#|\/|\\\|\&\~\*\{\+]/", $reg_username))
	{
		msgbox("error", "Ошибка!!!" ,"Введенное имя администратора недопустимо к регистрации!", "history.go(-1)");
	}
	$reg_password = md5($reg_password1);
/*	if( !$reg_password ) {
		msgbox("error", "Ошибка", "PHP extension Crypt must be loaded for password_hash to function", "history.go(-1)");
	}*/

	$url = htmlspecialchars( $url, ENT_QUOTES, 'ISO-8859-1');
	$reg_email = htmlspecialchars( $reg_email, ENT_QUOTES, 'ISO-8859-1');
	$url = str_replace( "$", "&#036;", $url );
	$reg_email = str_replace( "$", "&#036;", $reg_email );
	$dbhost = str_replace ('"', '\"', str_replace ("$", "\\$", $_POST['dbhost']) );
	$dbname = str_replace ('"', '\"', str_replace ("$", "\\$", $_POST['dbname']) );
	$dbuser = str_replace ('"', '\"', str_replace ("$", "\\$", $_POST['dbuser']) );
	$dbpasswd = str_replace ('"', '\"', str_replace ("$", "\\$", $_POST['dbpasswd']) );
	$dbprefix = str_replace ('"', '\"', str_replace ("$", "\\$", $_POST['dbprefix']) );

define ("DB_PRE", $dbprefix);
define ("DB_HOST", $dbhost);
define ("DB_USER", $dbuser);
define ("DB_PWD", $dbpasswd);
define ("DB_NAME", $dbname);

$ver = "1.0";
$sitename = "Livemon";
$home_title = "Демонстрационный сайт";
$tpl = "grape";
$description = "Демонстрационная страница движка Livemon CMS";
$keywords = "Livemon, CMS, PHP движок";
$home_mod = "main";
$sitelink = $url;
// $sitelink = 'https://'.$_SERVER['HTTP_HOST'].'/';
$config=RD . '/app/config.php';
//если файла нету... тогда
if( !file_exists($config)) {
$handler = fopen($config, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту), мы создаем файл
fwrite($handler, "<?php \n\n//System Configurations\n\n\$CONFIG = array (\n\n");
fwrite($handler, "'SITELINK' => \"$sitelink\",\n\n");
fwrite($handler, "'SITENAME' => \"$sitename\",\n\n");
fwrite($handler, "'HOME_TITLE' => \"$home_title\",\n\n");
fwrite($handler, "'DESCRIPTION' => \"$description\",\n\n");
fwrite($handler, "'KEYWORDS' => \"$keywords\",\n\n");
fwrite($handler, "'HOME' => \"$home_mod\",\n\n");
fwrite($handler, "'TPLDIR' => \"$tpl\",\n\n");
fwrite($handler, "'VERSION' => \"$ver\",\n\n");
fwrite($handler, "'DB_NAME' => \"$dbname\",\n\n");
fwrite($handler, "'DB_USER' => \"$dbuser\",\n\n");
fwrite($handler, "'DB_HOST' => \"$dbhost\",\n\n");
fwrite($handler, "'DB_PWD' => \"$dbpasswd\",\n\n");
fwrite($handler, "'DB_PRE' => \"$dbprefix\",\n\n");
fwrite($handler, ");\n\n?>" );
fclose ($handler);
}
require_once APP . '/core/registry.php';
require_once APP .'/libs/classes/database.php';
$DB = DataBase::getDB();

$DB->table_delete($table=DB_PRE.'_logs');
$columns="
  `id` int(11) NOT NULL auto_increment,
  `log_date` varchar(255) NOT NULL default '',
  `log_user` varchar(255) NOT NULL default 'N/A',
  `log_query` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
";
$DB->table_create($table=DB_PRE.'_logs', $columns);

$DB->table_delete($table=DB_PRE.'_modules');
$columns="
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL default 'N/A',
  `body` varchar(255) NOT NULL,
  `version` double NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
";
$DB->table_create($table=DB_PRE.'_modules', $columns);

$DB->table_delete($table=DB_PRE.'_menu');
$columns="
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default 'N/A',
  `title_url` varchar(255) NOT NULL,
  `parent` varchar(255) NOT NULL default 'none',
  PRIMARY KEY  (`id`)
";
$DB->table_create($table=DB_PRE.'_menu', $columns);
$DB->table_delete($table=DB_PRE.'_submenu');
$columns="
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default 'N/A',
  `title_url` varchar(255) NOT NULL,
  `parent` varchar(255) NOT NULL default 'none',
  PRIMARY KEY  (`id`)
";
$DB->table_create($table=DB_PRE.'_submenu', $columns);
$DB->table_delete($table=DB_PRE.'_users');
$columns="
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `name` varchar(70) NOT NULL,
  `surname` varchar(70) NOT NULL,
  `fullname` varchar(70) NOT NULL,
  `img` varchar(30) NOT NULL,
  `log_time` int(11) NOT NULL,
  `user_group` int(11) NOT NULL,
  `info` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `email` (`email`)
";
$DB->table_create($table=DB_PRE.'_users', $columns);
$DB->insert(DB_PRE.'_modules', array(
'name' => 'api', 
'title' => 'api', 
'body' => 'api', 
'version' => '1.0',
'status' => '1'));
$DB->insert(DB_PRE.'_modules', array(
'name' => 'dashboard', 
'title' => 'dashboard', 
'body' => 'dashboard', 
'version' => '1.0',
'status' => '1'));
$DB->insert(DB_PRE.'_modules', array(
'name' => 'main', 
'title' => 'main', 
'body' => 'main', 
'version' => '1.0',
'status' => '1'));
$DB->insert(DB_PRE.'_modules', array(
'name' => 'manager', 
'title' => 'manager', 
'body' => 'manager', 
'version' => '1.0',
'status' => '1'));
$DB->insert(DB_PRE.'_modules', array(
'name' => 'tools', 
'title' => 'tools', 
'body' => 'tools', 
'version' => '1.0',
'status' => '1'));

$DB->insert(DB_PRE.'_menu', array(
'title' => 'Главная', 
'title_url' => 'main/', 
'parent' => 'none'));

$username = $reg_username;
$password = $reg_password;
$email = $reg_email;
$name = 'Администратор';
$surname = 'Сайта';
$fullname = $name.' '.$surname;
$img = '';
$login_time = time();
$user_group = '1';
$info = '';

$DB->insert(DB_PRE.'_users', array(
'username' => $username, 
'password' => $password, 
'email' => $email,  
'name' => $name, 
'surname' => $surname, 
'fullname' => $fulname, 
'img' => $img, 
'log_time' => $login_time, 
'user_group' => $user_group, 
'info' => $info));

echo $skin_header.'<form action="index.php" method="get">
<div class="box">
  <div class="box-header">
    <div class="title">Установка завершена</div>
  </div>
  <div class="box-content">
	<div class="row box-section">
Поздравляем Вас, Livemon CMS был успешно установлен на Ваш сервер. Вы можете зайти теперь на главную <a class="status-info" href="/">страницу вашего сайта</a>  и посмотреть возможности скрипта. Либо Вы можете <a class="status-info" href="dashboard">зайти</a> в панель управления Livemon CMS и изменить другие настройки системы.
<br><br><font color="red">Внимание: при установке скрипта создается структура базы данных, создается аккаунт администратора, а также прописываются основные настройки системы, поэтому после успешной установки удалите файл <b>/app/install.php</b> во избежание повторной установки скрипта!</font><br><br>
Приятной Вам работы<br><br>
Livemon<br><br>
	</div>
	<div class="row box-section">
		<input class="btn btn-green" type=submit value="Продолжить">
	</div>
  </div>
</div>';

}else {

/* !!!                                            !         */
	if (@file_exists(APP.'/config.php')) {

		msgbox( "", "Установка скрипта автоматически заблокирована", "Внимание, на сервере обнаружена уже установленная копия Livemon CMS. Если вы хотите еще раз произвести установку скрипта, то вам необходимо вручную удалить файл <b>/app/config.php</b>, используя FTP протокол." );

		die ();
	}

$_SESSION['lm_install'] = 1;
// **
// Приветствие
// **

echo $skin_header.'<form method="get" action="">
<input type="hidden" name="action" value="eula">
<div class="box">
  <div class="box-header">
    <div class="title">Мастер установки Livemon CMS</div>
  </div>
  <div class="box-content">
	<div class="row box-section">
	Добро пожаловать в мастер установки Livemon CMS. Данный мастер поможет вам установить скрипт всего за несколько минут.<br><br>
	Прежде чем начать установку убедитесь, что все файлы дистрибутива загружены на сервер, а также выставлены необходимые права доступа для папок и файлов.<br><br>
	<font color="red">Внимание: при установке скрипта создается структура базы данных, создается аккаунт администратора, а также прописываются основные настройки системы, поэтому после успешной установки удалите файл <b>install.php</b> во избежание повторной установки скрипта!</font><br><br>
	Приятной Вам работы,<br><br>
	Livemon.ru
	</div>
	<div class="row box-section">
		<input class="btn btn-green" type=submit value="Начать установку">
	</div>
  </div>
</div>
</form>';
}


echo $skin_footer;