<?php
/* 
	Appointment: Главная страница
	File: index.php
	Author: Phakof
	Engine: Livemon
	Copyright: Алексеев Семен (с) 2017
	E-mail: p@qd2.ru
	URL: https://livemon.ru/
	Данный код защищен авторскими правами
*/
define('RD', dirname (__FILE__));

if (preg_match( '|^www\..*|', $_SERVER [ 'HTTP_HOST' ])) {
	header ( 'HTTP/1.0 301 Moved Permanently' );
	$url = trim ($_SERVER [ 'REQUEST_URI' ], '/');
	if(trim($_SERVER [ 'REQUEST_URI' ], '/') != '') $url .= '/';
	header('Location: '.$_SERVER [ 'HTTP_HOST' ]. $url);
	die();
}

define('APP', RD.'/app');
define('MOD', RD.'/modules');

require_once RD . '/app/bootstrap.php';
?>