    <header class="header_block">
        <div class="layout_header"></div>
        <div class="layout_header">
            <a href="/" onClick="Page.Go(this.href); return false;">
              <span class="logotype"><?=SITENAME ?></span>
            </a>
        </div>
    <div class="login_status">
    <?php if (LOGGED) : ?>
    <div class="forms">
        <form method="POST" enctype="multipart/form-data"
        class="ajax_form"  action="javascript:void(null);"
        onsubmit="call('<?= '/tools/logout' ?>')">
        <a href="/id<?=Registry::get('user')->id; ?>" onClick="Page.Go(this.href); return false;"><?= Registry::get('user')->username ?> </a>
        <a href="/messages" onClick="Page.Go(this.href); return false;" style="display: none;">IM</a>
          <input type="submit" class="deny" value="Выйти" />
        </form>
    </div>
    <?php else : ?>
    <div class="forms">
    <form method="POST" enctype="multipart/form-data"
    class="ajax_form"  action="javascript:void(null);"
    onsubmit="call('<?= '/tools/login' ?>')">     
      <b>Введите логин</b>
      <input type="text" name="user[username]"/>      
      <b>Введите пароль</b>
      <input type="password" name="user[password]"/>
      <input type="submit" class="confirm" value="Войти" />
    </form>
    </div>
    <?php endif ?>    
    </div> 
    <div class="notices">
<!--              -->
<?php
$notices = '';
if (!empty($this->notice['notice'])) {
    $notices .= "<div class='sub-notice notice'>".$this->notice['notice']."</div>";
}
if (!empty($this->notice['error'])) {
    $notices .= "<div class='sub-notice error'>".$this->notice['error']."</div>";
}
if (!empty($this->notice['access'])) {
    $notices .= "<div class='sub-notice access'>".$this->notice['access']."</div>";
}
echo $notices;
?>
    </div>
        <div class="layout_header"></div>
        <div class="layout_tab_bar">
        </div>
    </header>   
<div class="menu-container">
  <nav class="menu">
    <ul>
      <?php require_once APP . '/menu.php'; // Подключаем меню 0.1 ?>
    </ul>
  </nav>
</div>