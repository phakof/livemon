<title><?php echo $this->data['header']['title'] ?></title>
<meta charset="utf-8">
<meta name="language" content="ru" />
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width"> 
<meta name="description" content="<?php echo $this->data['header']['description'] ?>">
<meta name="keywords" content="<?php echo $this->data['header']['keywords'] ?>" />
<meta name="generator" content="livemon CMS (https://livemon.ru)" />
<link rel="shortcut icon" type="image/ico" href="/fav.png">
<link rel="stylesheet" href="/assets/<?php echo TPLDIR ?>/css/s.css"/>
<script async src="/assets/<?php echo TPLDIR ?>/js/lib.js"></script>    
<script async src="/assets/<?php echo TPLDIR ?>/js/main.js"></script>     