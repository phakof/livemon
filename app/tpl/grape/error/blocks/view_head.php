<title>404</title>
<meta charset="utf-8">
<meta name="language" content="ru" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Livemon - cовременная система управления сайтом">
<meta name="keywords" content="cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS" />
<meta name="generator" content="livemon CMS (https://livemon.ru)" />
<meta property="og:title" content="404">
<meta property="og:image" content="/favicon.png">
<meta property="og:description" content="Livemon - cовременная система управления сайтом">
<meta property="og:url" content="<?php echo SITENAME ?>">
<meta property="og:type" content="website">
<meta property="og:site_name" content="Livemon">
<link rel="stylesheet" href="/assets/<?php echo TPLDIR ?>/css/styles.css"/>
<link rel="shortcut icon" type="image/ico" href="/fav.png">
<script src="/assets/<?php echo TPLDIR ?>/js/lib.js"></script>		
<script src="/assets/<?php echo TPLDIR ?>/js/main.js"></script>      