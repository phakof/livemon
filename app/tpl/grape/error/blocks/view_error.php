<?php
//$u = rand(0,1);
//http://umori.li/anekdot.html?name=new+anekdot
	$wall = file_get_contents("http://www.umori.li/api/get?site=bash.im&name=bash&num=100"); // Отправляем запрос
	$wall = json_decode($wall); // Преобразуем JSON-строку в массив

	$r = rand(0,99);
?>
<style type="text/css">
.b_error{
	border: 1px solid #99b; 
	background: #f0f0ff; 
	padding: 5px; 
	font-family: 'Monaco', 'Liberation Mono', 'Droid Sans Mono', 'Ubuntu Mono', 'Lucida Console', 'Helvetica Neue', monospace; 
	font-size: 10pt;
	width: 95%;
	margin: auto;
    margin-top: 10px;
    margin-bottom: 20px;
}
.entry{width: 100%;margin: 0 auto;}
.entry-header{background-color: rgb(39, 29, 78);color: rgb(201, 165, 201);margin-bottom: 0;width: 88%;margin-bottom: 3rem;margin: auto;}
.entry-image{margin: .5rem;max-width: 40%;max-height: none;height: 14rem;float: right;    }
.entry-header-inner{margin-top: 32px;padding: 3rem 3rem 3rem 3rem;width: 60%;padding-top: 100px;transform: translateY(-22%);text-align: center;}
.entry-content{margin: auto;width: 85%;text-align: center;padding: 20px;background: #fff;}
</style>
<article class="entry">
	<header class="entry-header">
		<img class="entry-image" src="/assets/img/black_hole.jpg" alt="404 image">
		<div class="entry-header-inner">
		<?php  
if (isset($title)) {
echo '<h1 class="entry-title">'.$title.'</h1>';
}else{
	echo '<h1 class="entry-title">Не найдено (404).</h1>';
}
		?>
			
		</div>
	</header>
	<div class="entry-content">
		<p>Страница, которую вы ищете, больше не существует. Возможно стоит вернуться на <a href="/" style="background: #f8fb3b;">главную</a>.</p>
	</div>
</article>

<h1 class="title">Случайная цитата</h1>
<div class="b_error">
	<div id="b_q">
		<div id="b_q_t" style="padding: 1em 0;">
<?php echo $wall[$r]->elementPureHtml; ?>
		</div>
	</div>
</div>
