<?php
class ControllerApi extends Controller
{

    public function actionIndex()
    {
        $this->view->data['api'] = $this->model->main();
        $this->view->content = array('api');
        $this->view->generate();
    }

    public function actionStyle()
    {
        // $this->view->data['css'] = $this->model->main();
        $this->view->content = array('css');
        $this->view->generate();
    }

    protected function access()
    {
        return array(
            'index'       =>array('*'),
        );
    }
}
