<?php
class ControllerDashboard extends Controller
{
    public function actionIndex()
    {
        //$sections = Logs::cp($query='Вход на главную');

        $this->view->data['news'] = $this->model->news();
        $this->view->data['logs'] = $this->model->logs();
        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'dashboard',$language='ru');
        $this->view->content = array('dashboard');
        $this->view->generate();
    }

    public function actionUsers()
    {
        $this->view->data['header'] = $this->model->header($content='index');

        $this->view->data['users_all'] = $this->model->all_users();
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'users',$language='ru');
        $this->view->content = array('cp_users');
        $this->view->generate();
    } 

    public function actionMenu()
    {
        if (isset(Router::$params[2])) {
            if (Router::$params[2] == 'up') {
                $this->model->menu_up(Router::$params[3]);
                header("Location: /dashboard/menu");
            }elseif (Router::$params[2] == 'down') {
                $this->model->menu_down(Router::$params[3]);
                header("Location: /dashboard/menu");
            }elseif (Router::$params[2] == 'del') {
                $this->model->menu_del(Router::$params[3]);
                header("Location: /dashboard/menu");
            }
        }

        $this->view->data['header'] = $this->model->header($content='index');

        // $this->view->data['menu'] = $this->model->all_menu();
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'menu',$language='ru');

        $this->view->content = array('menu');
        $this->view->generate();
    } 

    public function actionClear_log()
    {
        $this->model->clear_log();
        header("Location: /dashboard/");
    } 

    public function actionSystem()
    {
        $this->view->data['info'] = $this->model->info();
        
        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'system',$language='ru');
        $this->view->content = array('system');
        $this->view->generate();
    }       

    public function actionSettings()
    {
        $this->view->data['save'] = $this->model->settings_get();
        if(isset($_POST['save_conf'])) {
            $saves = $_POST['save'];
            $this->model->settings_save_conf($saves);
            Logs::go($query="Изменены настройки сайта");
        }elseif(isset($_POST['save_bd'])){
            $saves = $_POST['savebd'];
            $this->model->settings_save_bd($saves);
            Logs::go($query="Изменены настройки DB");
        }
        
        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'settings',$language='ru');
        $this->view->content = array('settings');
        $this->view->generate();   
    }    

    protected function access()
    {
        return array(
            'index'       =>array('A'),
            'system'       =>array('A'),            
        );
    }
}