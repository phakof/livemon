<?php
class ControllerTools extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->view->data['header'] = $this->model->header($content='index');
    }
    
    public function actionLogin()
    {
        $this->view->data['header'] = $this->model->header($content='index');
        if (isset($_POST)) {
            if (isset($_POST['user'])) {
                if ($this->view->user = $this->model->login($_POST['user'])) {
                    $this->view->notice['notice'] = 'Вы успешно авторизованны!';
                } else {
                    $this->view->notice['error'] = $this->model->error;
                }
                $this->view->jsonResponse();
            } else {
                $this->view->viewScheme['content'][S]['login'] = array();
                $this->view->generate();
            }  
        }else{
          header("Location: /");
        }

    }

    public function actionlog_in()
    {
        $this->view->data['header'] = $this->model->header($content='index');

        $this->view->content = array('main');
        $this->view->generate();
    }    
    
    public function actionLogout()
    {
        unset($_SESSION['usr']);
    	session_destroy();
        $this->view->notice['notice'] = 'Вы вышли. Данные сессии удалены.';
        $this->view->jsonResponse();
    }
    
    protected function access()
    {
        return array(
            'index'    =>array('A'),
            'register' =>array('*'),
            'login'    =>array('*'),
            'log_in'    =>array('*'),
            'logout'   =>array('*'),
            'migrate'   =>array('A'),
        );
    }
}