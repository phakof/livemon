<?php
class ControllerUpdate extends Controller
{
    public function actionIndex()
    {
        $this->view->data['header'] = $this->model->header($content='index');

        $this->view->content = array('updatea');
        $this->view->generate();
    }

    public function actionStep1()// шаг 1 # получили данные SYSTEM
    {
        $this->model->step1();
        // $this->model->step($step0=1);
        header("Location: /update/step2");
    }

    public function actionStep1_mod()// шаг 1 # получили данные MOD
    {
        $addon=Router::$params[2];
        $version=Router::$params[3];
        $path = $this->model->step1_mod($addon, $version);
        header("Location: /update/step2_mod/".$addon);
    }

    public function actionStep2_mod()// шаг 2 # распаковываем MOD
    {
        $addon=Router::$params[2];
        $result = $this->model->step2_mod($addon);
        if ($result == true) {
            header("Location: /update/step3_mod"); 
        }else{
            header("Location: /update/finish?error=2"); 
        }
    }

    public function actionStep3_mod()// шаг 3 # удаляем/копируем MOD
    {
        $addon=Router::$params[2];
        $result = $this->model->step3_mod($addon);
        if ($result == true) {
            // $this->model->step($step0=3);
            header("Location: /update/step4_mod".$addon); 
        }else{
            header("Location: /update/finish?error=3"); 
        }        
    }

    public function actionStep4_mod()// шаг 4 # запросы в бд MOD
    {
        $addon=Router::$params[2];
        $result = $this->model->step4_mod($addon);
        if ($result == true) {
            // $this->model->step($step0=4);
            header("Location: /update/step5_mod".$addon); 
        }else{
            header("Location: /update/finish?error=4"); 
        }
    }

    public function actionStep5_mod()// шаг 5 # Завершение MOD
    {
        $addon=Router::$params[2];
        $result = $this->model->step5_mod($addon);
        if ($result == true) {
            // $this->model->step($step0=5);
            Logs::go($query="Обновлен модуль ".$addon);
            header("Location: /update/finish"); 
        }else{
            header("Location: /update/finish?error=5"); 
        }
    }

    public function actionStep2()// шаг 2 # распаковываем
    {
        $result = $this->model->step2();
        if ($result == true) {
            // $this->model->step($step0=2);
            header("Location: /update/step3"); 
        }else{
            header("Location: /update/finish?error=2"); 
        }
    }

    public function actionStep3()// шаг 3 # удаляем/копируем
    {
        $result = $this->model->step3();
        if ($result == true) {
            // $this->model->step($step0=3);
            header("Location: /update/step4"); 
        }else{
            header("Location: /update/finish?error=3"); 
        }        
    }

    public function actionStep4()// шаг 4 # запросы в бд
    {
        $result = $this->model->step4();
        if ($result == true) {
            // $this->model->step($step0=4);
            header("Location: /update/step5"); 
        }else{
            header("Location: /update/finish?error=4"); 
        }
    }

    public function actionStep5()// шаг 5 # удаляем обновлятор
    {
        $result = $this->model->step5();
        if ($result == true) {
            // $this->model->step($step0=5);
            Logs::go($query="Обновлена система");
            header("Location: /update/finish"); 
        }else{
            header("Location: /update/finish?error=5"); 
        }
    }

    public function actionFinish()// шаг 6 # Finish
    {
        // $this->model->step($step0=6);
        $this->view->data['header'] = $this->model->header($content='index');
        if (isset($_GET['error'])) {
            $error = $_GET['error'];
            $this->view->data['stop'] = $error;
            $this->view->content = array('update_error');            
        }else{
            $this->view->content = array('updateb');            
        }
        $this->view->generate();
    }

    protected function access()
    {
        return array(
            'index'       =>array('A'),
        );
    }
}