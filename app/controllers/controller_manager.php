<?php
class ControllerManager extends Controller
{
    public function actionIndex()
    {
        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'manager',$language='ru');
        $this->view->content = array('manager');
        $this->view->generate();
    } 

    public function actionModules()
    {
        $this->view->data['mods_all'] = $this->model->all_modules();
        
        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'modules',$language='ru');
        $this->view->content = array('manager_modules');
        $this->view->generate();
    }

    public function actionAll_modules()// модули из магазина
    {
        $this->view->data['addons'] = $this->model->addons($type='mod');

        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'modules',$language='ru');
        $this->view->content = array('manager_all_modules');
        $this->view->generate();
    }

    public function actionAll_templates()// Шаблоны из магазина
    {
        $this->view->data['addons'] = $this->model->addons($type='tpl');

        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'modules',$language='ru');
        $this->view->content = array('manager_all_templates');
        $this->view->generate();
    }

    public function actionDownload()// скачать из магазина
    {
        $this->view->data['header'] = $this->model->header($content='index');

        if (isset($_GET['url']) AND isset($_GET['name']) AND isset($_GET['addon']) ) {

            $url = $_GET['url'];$name = $_GET['name'];$addon = $_GET['addon'];
            $result = $this->model->download($url,$name,$addon);
            if ($addon == 'tpl') {
                Logs::go($query="Добавлен шаблон ".$name);
                header("Location: /manager/templates");
            }elseif ($addon == 'mod') {
                Logs::go($query="Добавлен модуль ".$name);
                header("Location: /manager/update");
            }
        }
    }

    public function actionInstall_mod()// Установить мод
    {
        if (isset($_GET['name'])) {
            $mod_name = $_GET['name'];
            //$this->view->data['update_mods_list'] = $this->model->modules_update();
            $install_mod = $this->model->install_mod($mod_name);
            Logs::go($query="Установлен модуль ".$mod_name);
        header("Location: /manager/modules");
        }
    } 

    public function actionDelete_mod()// Удалить мод
    {
        if (isset($_GET['name'])) {
            $mod_name = $_GET['name'];
            $install_mod = $this->model->delete_mod($mod_name);
            Logs::go($query="Удален модуль ".$mod_name);
        header("Location: /manager/modules");
        }
    } 

    public function actionUpdate()// Обновить модули
    {
        $this->view->data['all_modules'] = $this->model->modules_update();
        
        $this->view->data['header'] = $this->model->header($content='index');
        header("Location: /manager/modules");
    } 

    public function actionTemplates() // Шаблоны
    {
        $this->view->data['all_tpl'] = $this->model->all_templates();

        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'templates',$language='ru');
        $this->view->content = array('manager_templates');
        $this->view->generate();
    } 

    public function actionInstall_tpl_loc()// Установить шаблон
    {
        if (isset(Router::$params[2])) {
            $tpl = Router::$params[2];
            $this->model->install_tpl_loc($tpl);
            Logs::go($query="Установлен шаблон");

        }        
        header("Location: /manager/templates");
/*        $this->view->data['header'] = $this->model->header($content='index');

        $this->view->data['rendermenu'] = $this->model->renderMenu($page = 'templates',$language='ru');

        $this->view->data['all_tpl'] = $this->model->all_templates();

        $this->view->content = array('manager_templates');
        $this->view->generate();*/
    }

    public function actionDownload_tpl()// скачать шаблон из магазина !NB
    {
        $this->view->data['header'] = $this->model->header($content='index');
        if (isset($_GET['url']) AND isset($_GET['name']) AND isset($_GET['addon']) ) {
            $url = $_GET['url'];$name = $_GET['name'];$addon = $_GET['addon'];
            $result = $this->model->download($url,$name,$addon);
        }
            Logs::go($query="Добавлен шаблон");
        header("Location: /manager/templates");
    }

    protected function access()
    {
        return array(
            'index'       =>array('A'),  
            'install_mod'       =>array('A'),               
            'update'       =>array('A')                   
        );
    }
} 