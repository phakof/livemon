<?php
class ControllerMain extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->view->data['header'] = $this->model->header($content='index');
    }

    public function actionIndex()
    {
    	if (HOME !== 'main') {
        header("Location: /".HOME."/");
    	}
        $this->view->data['header'] = $this->model->header($content='index');
        $this->view->content = array('main');
        $this->view->generate();
    }

    protected function access()
    {
        return array(
            'index'       =>array('*'),
        );
    }
}