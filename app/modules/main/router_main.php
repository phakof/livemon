<?php  
/**
* MAIN
*/
class routermain
{

/*	function __construct(argument)
	{
		# code...
	}*/
    static $routes = array();
    static $params = array();
    static $title = '';
    static $int = '';

    static $ctrlName = 'users';
    static $modName = 'main';
    static $actionName = 'index';

    public static function start($r, $to_route='')
    {
        if (isset($to_route)) {
            // $r;
            $to_r = $to_route;
        }
        self::$params = $to_route;
        self::$routes = $r;

        $r = self::$routes[0];// id(int)

        self::$int = substr($r, 2); 
        self::$modName = 'main';

        self::getCtrlNames();
        
        self::model();
        self::controller();
        return true;
	}

    protected static function controller()
    {   
        $ctrlFName = 'controller_users';
        $ctrlFile = $ctrlFName.'.php';

        if (defined('RMOD')) {
            $ctrlPath = RMOD.'/controllers/'.$ctrlFile;
        }

        if(file_exists($ctrlPath)) {
            require_once strtolower($ctrlPath);
        } else {
        	echo "<br> контроллер ".$ctrlFile." не найден, id = ".self::$int;
            self::e_404();
        }

        $ctrlFName = str_replace('_', '', $ctrlFName);
        $ctrl = new $ctrlFName();
        $action = 'action'.self::$actionName;
        
        if(method_exists($ctrl, $action)) {
            if ( !empty(self::$routes[3])) {
                $ctrl->$action(self::$routes[3]);
            } else {
                $ctrl->$action();
            }
        } else {
            self::e_404(self::$title);
        }
    }

    protected static function getCtrlNames($mod = 0)
    {
        $_ctrl = 0 + $mod;
        $_act  = 1 + $mod;

        if (!empty(self::$routes[$_ctrl])) {
            self::$ctrlName = str_replace("-","_",self::$routes[$_ctrl]);
        }

        if (!empty(self::$routes[$_act])) {
            self::$actionName = str_replace("-","_",self::$routes[$_act]);
        }
            define('RMOD', RD.'/application/modules/main/'); #! main
     }

    protected static function model()
    {

        $modelName = 'model_users';
        $modelFile = $modelName.'.php';
        
        if (defined('RMOD')) {
            $modelPath = RMOD.'/models/'.$modelFile;
        }

        if(file_exists($modelPath)) {
            require_once strtolower($modelPath);
            return true;
        } else{
            echo "<br> контроллер ".$modelFile." не найден, id = ".self::$int;
            return false;
        } 

    }

    public static function e_404($title='')
    {

@ini_set ( 'display_errors', false );
@ini_set ( 'html_errors', false );
        if(AJAX == false){
        header('HTTP/1.1 404 Not Found');            
        }
        //header("Status: 404 Not Found");
        require_once RD . '/tpl/'.TPLDIR.'/error/main.php';
        //header('Location:'.$host.'404');
    }
}



?>