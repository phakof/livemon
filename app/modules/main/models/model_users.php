<?php
class ModelMain extends Model
{
    function __construct() {
        parent::__construct();
    }

	public function header($content)
	{
		if ($content=='index') {
			$header = array(
				'title' => Registry::get('user')->name.' '.Registry::get('user')->surname, 
				'description' => Registry::get('user')->name.' '.Registry::get('user')->surname.' - страница',
				'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
				);		
		} elseif($content=='') {
			$header = array(
				'title' => Registry::get('user')->name.' '.Registry::get('user')->surname, 
				'description' => Registry::get('user')->name.' '.Registry::get('user')->surname.' - страница',
				'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
				);	
		}
		return $header;
	}

	public function user($id)
	{
        $query = "SELECT * FROM ".DB_PRE."_users WHERE `id`={?} ";
        $params = array($id);
        $user = $this->DB->selectRow($query, $params);

		return $user;
	}

	public function ava_get($id)
	{
        $query = "SELECT * FROM ".DB_PRE."_users WHERE `id`={?} ";
        $params = array($id);
        $users = $this->DB->selectRow($query, $params);
        // $img = $users['img'];
        return $users;
	}

	public function ava_update($files=array(), $id, $users=array())
	{
		$uploaddir = RD . '/uploads/users/';
		$path = $uploaddir.$id;
		if(!is_dir($path)){ 
			@mkdir($path, 0777 );
			@chmod($path, 0777 );
		}
		$server_time = intval($_SERVER['REQUEST_TIME']);

		//Получаем данные о фотографии
		$image_tmp = $files['tmp_name'];
		// $image_name = totranslit($files['name']); // оригинальное название для оприделения формата
		$image_name = $files['name']; // оригинальное название для оприделения формата
		$image_rename = substr(md5($server_time+rand(1,100000)), 0, 20); // имя фотографии
		$image_size = $files['size']; // размер файла
		$type = end(explode(".", $image_name)); // формат файла
		
		$photo_format = "jpg, jpeg, jpe, png, gif";
		$allowed_files = explode(', ', $photo_format);

		if(in_array(strtolower($type), $allowed_files)){
			$res_type = strtolower('.'.$type);
			if(move_uploaded_file($image_tmp, $path.$image_rename.$res_type)){
				// Images::size_auto('770');
				// Images::jpeg_quality('85');
				if (!@copy($files['tmp_name'], $path . $files['name'])){
					// echo 'Что-то пошло не так';
				}
				// echo $res;				
		        $where = "id = " . $id;
		        $this->DB->update('users', $where, array(
		            //'id' => $post_id,
		            'lm_id' => $users['lm_id'], 
		            'username' => $users['username'], 
		            'password' => $users['password'], 
		            'email' => $users['email'], 
		            'name' => $users['name'], 
		            'surname' => $users['surname'], 
		            'fio' => $users['fio'], 
		            'img' => $image_rename, 
		            'status' => $users['status'], 
		            'login_tome' => $users['login_tome'], 
		            'user_group' => $users['user_group'], 
		            'info' => $users['info'], 
		        ));
		        header("Location: /id".$id);
       			return true;
			} else{
				// echo 'big_size';
            routermain::e_404($title='пользователь не найден');
				return false;
			}
		} else {
			// echo 'bad_format';
            routermain::e_404($title='пользователь не найден');
			return false;
		}
	}	
}