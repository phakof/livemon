<?php
class ControllerUsers extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->view->data['header'] = $this->model->header($content='index');
    }

    public function actionIndex()
    {
        $this->view->data['header'] = $this->model->header($content='index');

        $r = routermain::$routes[0];// id(int)
        $id = substr($r, 2); 

		if (LOGGED) {
		    if ($id == Registry::get('user')->id) {
		        $this->view->data['owner']=true;
		    }else{
		        $this->view->data['owner']=false;
		    }
		}else{
		        $this->view->data['owner']=false;
		}


        $user = $this->model->user($id);

        if ($user !== false) {
            $this->view->data['user'] = $user;
            $this->view->content = array('users');
            $this->view->generate();
            
        }else{
            routermain::e_404($title='пользователь не найден');
        }
        
        // var_dump($this->view->data['user']);

        // $this->view->data['info_api'] = $this->model->api();
    }

    public function actionEdit()
    {
        $this->view->data['header'] = $this->model->header($content='index');

        $r = routermain::$routes[0];// id(int)
        $id = substr($r, 2); 

        if ($id == Registry::get('user')->id) {
            $this->view->data['owner']=true;
        }else{
            $this->view->data['owner']=false;
        }

        $user = $this->model->user($id);

        if ($user !== false) {
            $this->view->data['user'] = $user;
            $this->view->content = array('edit');
            $this->view->generate();
            
        }else{
            routermain::e_404($title='пользователь не найден');
        }
    }

    public function actionUpload()
    {

        $r = routermain::$routes[0];// id(int)
        $id = substr($r, 2); 
    	
        if (isset($_FILES['data'])) {
			$ava = $this->model->ava_get($id);
/*			if ($ava) {
				# code...
			}*/
            $files = array();
            $files['data'] =              $_FILES['data'];
            $files['data']['name'] =      $_FILES['data']['name'];
            $files['data']['tmp_name'] =  $_FILES['data']['tmp_name'];
            $files['data']['size'] =      $_FILES['data']['size'];
			$this->model->ava_update($files['data'], $id, $ava);
			header("Location: /id".$id);
        }else{
            routermain::e_404($title='пользователь не найден');
        }
        
    }

    protected function access()
    {
        return array(
            'index'       =>array('*'),
        );
    }
}