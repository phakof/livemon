<?php
class Router
{
    static $routes = array();
    static $routesm = array();
    static $ctrlName = HOME;
    static $actionName = 'index';
    static $params = array();

    
    public static function start()
    {
        $r = (!empty($_GET['routes'])) ? $_GET['routes'] : '';

        self::$routes = explode('/', $r);
        $params = self::$routes;
        unset(self::$params[0], self::$params[1]);
        switch (self::$routes[0]) {
            case 'module1': case 'module2':
            unset(self::$params[2]);
            self::$params = array_values($params);
            self::module();
            break;
        default:

            $rt = self::$routes[0];
            self::$routesm = self::$routes;
            $count = strlen($rt);
            $id = substr($rt, 0, 2);
            $int = substr($rt, 2); 
            $int1 = $int + 1 - 1;
            if ($int1 == $int) {
                $value = true;
            }
            // $value = is_int($int);
            if ($value == true AND $id=='id' /*AND $count > 2*/) {
                unset(self::$params[2]);
                self::$params = array_values($params);
                self::module($smod='main');
            }/*elseif ($rt == 'news') {
                unset(self::$params[2]);
                self::$params = array_values($params);
                self::module();
            }*/
            else{
                self::$params = array_values($params);
                self::getCtrlNames();
                self::model();
                self::controller();    
            }

        }

    }
    
    protected static function getCtrlNames($mod = 0)
    {
        $_ctrl = 0 + $mod;
        $_act  = 1 + $mod;

        if (!empty(self::$routes[$_ctrl])) {
            self::$ctrlName = str_replace("-","_",self::$routes[$_ctrl]);
        }

        if (!empty(self::$routes[$_act])) {
            self::$actionName = str_replace("-","_",self::$routes[$_act]);
        }

        if (self::$ctrlName == 'register' || self::$ctrlName == 'api' || self::$ctrlName == 'dashboard' || self::$ctrlName == 'users' || self::$ctrlName == 'main' || self::$ctrlName == 'manager' || self::$ctrlName == 'tools' || self::$ctrlName == 'update') {
            if (self::$ctrlName == 'dashboard' || self::$ctrlName == 'manager' || self::$ctrlName == 'tools' || self::$ctrlName == 'update') {
                define('CP', 'true');
                
            }
            define('SYSMOD', RD.'/app');
        }
    }
    protected static function controller()
    {   
        $ctrlFName = 'controller_'.self::$ctrlName;
        $ctrlFile = $ctrlFName.'.php';
        if (defined('SYSMOD')) {
            $ctrlPath = SYSMOD.'/controllers/'.$ctrlFile;
        } else {
            $ctrlPath = MOD . '/'.self::$ctrlName. '/controllers/'.$ctrlFile;//APP
        }
        if(file_exists($ctrlPath)) {
            require_once strtolower($ctrlPath);
        } else {
            self::e_404();
        }

        $ctrlFName = str_replace('_', '', $ctrlFName);
        $ctrl = new $ctrlFName();
        $action = 'action'.self::$actionName;
        
        if(method_exists($ctrl, $action)) {
            if ( !empty(self::$routes[3])) {
                $ctrl->$action(self::$routes[3]);
            } else {
                $ctrl->$action();
            }
        } else {
            self::e_404();
        }
    }
    protected static function model()
    {
        $modelName = 'model_'.self::$ctrlName;
        $modelFile = $modelName.'.php';
        if (defined('SYSMOD')) {
            $modelPath = SYSMOD.'/models/'.$modelFile;
        } else {
            $modelPath = MOD . '/'.self::$ctrlName.'/models/'.$modelFile;//APP
        }
        if(file_exists($modelPath)) {
            require_once strtolower($modelPath);
            return true;
        } else return false;
    }
    
    protected static function module($smod='')
    {
// site.ru/[0]/[1]/[2]
// $routes[0]

/*        if (self::$routes[0] == 'users' || self::$routes[0] == 'dashboard') {
            define('SYSMOD', RD.'/application');
        }*/
            // echo "модуль не найден.";

        if (!empty(self::$routes[0])) { 
            // $r = self::$routes[0];
            if ($smod !=='') {
                $to_route = $smod;
                self::$routes[0] = $to_route;
            }elseif ($smod =='main') {
                $to_route = $smod;
                //self::$routes[0] = $to_route;
            }else{
                $to_route = self::$routes[0];
            }

            $module = $to_route;
            $moduleClass = 'router_'.$module;
            define('SMOD', APP.'/modules/'.$module);
            $path = SMOD.'/'.$moduleClass.'.php';
        }
        if(file_exists($path)) {
            require_once strtolower($path);
            $moduleClass = str_replace('_', '', $moduleClass);
            $title = $moduleClass::start(self::$routesm, $to_route);
        } else {
        $title =  'Пользователь № '.$int.' не найден';

            self::e_404($title);
        }
    }
    
    public static function e_404($title='')
    {

@ini_set ( 'display_errors', false );
@ini_set ( 'html_errors', false );
        //$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        if(AJAX == false){
        // header('HTTP/1.1 404 Not Found');
            header( 'Status: 403 Forbidden' );
            header( 'HTTP/1.1 403 Forbidden' );         
        }
        //header("Status: 404 Not Found");
        require_once APP . '/tpl/'.TPLDIR.'/error/main.php';
        //echo '<br>Ошибка 404<br><br>Страница не найдена';
        //header('Location:'.$host.'404');
    }
}