<?php
class View
{
    //public $test;
    
    public $blocks  = array();
    public $content = array();
    public $layout  = 'main';
    
    public $user   = array();
    public $site   = array();
    public $data   = array();
    public $notice = array(
        'error'=>'',
        'notice'=>''
        );
    
    public function __construct() {
        $this->site = Registry::get('site');
        $this->user = Registry::get('user');
        
        $this->blocks = array(
            'head'   => array('head'),
            'header' => array('header'),
            'footer' => array('footer'),
            // 'modal'  => array('modal'),
        );
        if (defined('CP')) {
            unset($this->blocks['header']['header']);
            $this->blocks['header']=array('cpheader');
        }
    }
    
    public function content() {
        foreach ($this->content as $k => $name) {
        if (defined('RMOD')) {
            $this->inclPath(RMOD.'/views/'.TPLDIR.'/view_'.$name.'.php', true);
        }elseif (defined('SYSMOD')) {
            $this->inclPath(SYSMOD.'/views/'.TPLDIR.'/view_'.$name.'.php', true);
        } else {
            $this->inclPath(MOD.'/'.Router::$ctrlName.'/views/view_'.$name.'.php', true);
        }
        }
    }
    
    public function block($tpl) {
        if (is_array($tpl)) {
            foreach ($tpl as $k => $name) {
                $this->inclPath(APP.'/tpl/'.TPLDIR.'/blocks/view_'.$name.'.php');
            }
        } else {
            $this->inclPath(APP.'/tpl/'.TPLDIR.'/blocks/view_'.$tpl.'.php');
        }
    }
    
    private function inclPath($path, $extract = false) {
        if (file_exists($path)) {
            if ($extract) {// преобразуем элементы массива в переменные
                extract($this->data);
            }
            require_once $path;
        } else {
            echo '<br>[ FILE < '.$path.' > IS NOT EXISTS ]';
        }
    }
    
    function generate() {
        require_once APP.'/tpl/'.TPLDIR.'/'.$this->layout.'.php';
    }
    
    public function htmlResponse($name, $block = 'modal') {
        if (defined('SYSMOD')) {
            $this->inclPath(SYSMOD.'/views/'.TPLDIR.'/view_'.$name.'.php', true);
        } else {
            $this->inclPath(MOD.'/'.Router::$ctrlName.'/views/view_'.$name.'.php', true);
        }
    }
    
    public function jsonResponse() {
        $response = array(
            'data'=>$this->data,
            'notice'=>$this->notice['notice'],
            'error'=>$this->notice['error'],
        );
        echo json_encode($response, JSON_FORCE_OBJECT);
    }   
}