<div>
	<div class="left_bar">
	<h3 class="pl10">Меню</h3>
		<div>
<?php echo $rendermenu; ?>
		</div>
	</div>
	<div class="cp_body">
		<h2 class="tcp title">
<a href="/dashboard/" onClick="Page.Go(this.href); return false;">Обзор</a>
		</h2>

<style>
section > a:hover{
 text-decoration: none;
}
</style>
<div class="cp">
	<section style="height: 220px;margin-bottom: 20px;width: 150px;">
	<a href="/manager/modules/" onClick="Page.Go(this.href); return false;">
		<div class="img" style="height: 150px;margin: auto;width: 150px;">
			<img src="/assets/grape/img/modules.png" style="width: 100%;" alt="Модули">
		</div>
	<h2>Модули</h2>				
		<div class="text-j">
			<p>	Расширение функционала</p>
		</div>
	</a>
	</section>
	<section style="height: 220px;margin-bottom: 20px;width: 150px;">
	<a href="/manager/templates/" onClick="Page.Go(this.href); return false;">
		<div class="img" style="height: 150px;margin: auto;width: 150px;">
			<img src="/assets/grape/img/templates.png" style="width: 100%;" alt="Шаблоны">
		</div>
	<h2>Шаблоны</h2>				
		<div class="text-j">
			<p>	Оформление и дизайн</p>
		</div>
	</a>
	</section>
	<section style="height: 220px;margin-bottom: 20px;width: 150px;">
	<a href="/dashboard/users/" onClick="Page.Go(this.href); return false;">
		<div class="img" style="height: 150px;margin: auto;width: 150px;">
			<img src="/assets/grape/img/users.png" style="padding: 8px;width: 90%;" alt="Пользователи">
		</div>
	<h2>Пользователи</h2>				
		<div class="text-j">
			<p>	Пользователи</p>
		</div>
	</a>
	</section>
	<section style="height: 220px;margin-bottom: 20px;width: 150px;">
	<a href="/dashboard/system/" onClick="Page.Go(this.href); return false;">
		<div class="img" style="height: 150px;margin: auto;width: 150px;">
			<img src="/assets/grape/img/system.png" style="padding: 8px;width: 90%;" alt="Система">
		</div>
	<h2>Система</h2>				
		<div class="text-j">
			<p>	Система</p>
		</div>
	</a>
	</section>
	<section style="height: 220px;margin-bottom: 20px;width: 150px;">
	<a href="/dashboard/menu/" onClick="Page.Go(this.href); return false;">
		<div class="img" style="height: 150px;margin: auto;width: 150px;">
			<img src="/assets/grape/img/menu.png" style="width: 100%;" alt="Редактор меню">
		</div>
	<h2>Меню</h2>				
		<div class="text-j">
			<p>	Редактор меню</p>
		</div>
	</a>
	</section>

	<!-- 			<section style="width: 400px;margin-bottom: 20px;">
	<h2>Пользователи</h2>
	<div class="text-j">
	<p>	Сдесь еще ничего нет</p>
	<p>	Сдесь еще ничего нет</p>
	<p>	Сдесь еще ничего нет</p>
	<p>	Сдесь еще ничего нет</p>
	</div>
	</section>
	<section style="width: 400px;margin-bottom: 20px;">
	<h2>Модули</h2>
	<div class="text-j">
	<p>	Сдесь еще ничего нет</p>
	<p>	Сдесь еще ничего нет</p>
	<p>	Сдесь еще ничего нет</p>
	<p>	Сдесь еще ничего нет</p>
	</div>
	</section> -->	
</div>
		<div class="cp">

			<section style="width: 440px;margin-bottom: 20px;">
			<div style="display: flex;justify-content: space-around;align-items: center;">
				<h2>Журнал событий</h2>
				<a href="/dashboard/clear_log" onClick="Page.Go(this.href); return false;">Очистить</a>
			</div>
			
				<div class="text-j" style="max-height: 108px;overflow-x: auto;width: 95%;">
<?php echo $logs; ?>
				</div>
			</section>		

			<section style="width: 440px;margin-bottom: 20px;">
			<h2>Новости</h2>
				<div class="text-j" style="max-height: 108px;overflow-x: auto;width: 95%;">
<?php echo $news; ?>
				</div>
			</section>						

		</div>		
	</div>	

</div>


