<style>
.miniature_box {
    position: fixed;
    left: 0px;
    top: 0px;
    right: 0px;
    bottom: 0px;
    overflow: auto;
    padding-bottom: 20px;
    background: rgba(0, 0, 0, 0.8);
    z-index: 100;
}
.miniature_pos {
    width: 645px;
    margin: auto;
    background: #fff;
    padding: 20px;
    margin-top: 100px;
    box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.345);
    -moz-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.345);
    -webkit-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.345);
    font-size: 13px;
}
.apps_box_text {
    font-size: 12px;
}
.miniature_title {
    color: #415A77;
    font-size: 15px;
    font-weight: 700;
    margin-bottom: 10px;
}
.fl_l {
    float: left;
}
.lang_selected {
    background: #f5f5f5;
    color: #777;
    cursor: default;
}
.lang_but {
    padding: 10px;
    color: #426A87;
    font-weight: bold;
    cursor: pointer;
    font-size: 11px;
    border-radius: 3px;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    margin-top: 3px;
}
div > button{
margin-top: 5px;
}
</style>
<div class="miniature_box">
 <div class="miniature_pos" style="width:500px">
  <!-- <div class="miniature_title fl_l apps_box_text">Регистрация</div> -->
  <div class="clear"></div>

<link rel="stylesheet" href="/assets/<?php echo TPLDIR ?>/css/form.css">
<!-- <h1 class="title">Регистрация</h1> -->
<div class="form_continer">
    <form class="form_reg" autocomplete="off">
       <h1 class="form_reg_title" style="margin-left: -25px;">Регистрация</h1>
        <div class="form_reg_body">                              
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="data[username]" placeholder="user2017" min="6" autocomplete="off" autofocus required  id="username" class="form-control">
                    <label for="username" class="blmd-label">Имя</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="email" name="data[email]" placeholder="user@gmail.com" min="6" autocomplete="off" required  id="email" class="form-control">
                    <label for="email" class="blmd-label">Почта</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="password" name="data[password]" autocomplete="off" required  id="password" class="form-control">
                    <label for="password" class="blmd-label">пароль</label>
                </div>
            </div>
        </div>
        <div class="form_reg_body text-center">
            <button type="submit" formmethod="post" formaction="" class="btn btn-blmd ripple-effect btn-success btn-lg btn-block">Регистрация</button>
            <button type="reset" class="btn btn-blmd ripple-effect btn-success btn-lg btn-block">Очистить</button>
        </div>
    </form>
</div>
  <div class="clear" style="margin-top:15px"></div>
  <div class="button_div fl_r"><button onclick="lmBox.clos('lm_lang_box', 1)">Отмена</button></div>
 </div>
 <div class="clear" style="height:50px"></div>
</div>