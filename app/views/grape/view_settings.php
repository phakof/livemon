<link rel="stylesheet" href="/assets/<?php echo TPLDIR ?>/css/form.css">
<div>
	<div class="left_bar">
	<h3 class="pl10">Меню</h3>
		<div>
<?php echo $rendermenu; ?>			
		</div>
	</div>
	<div class="cp_body">
        <h2 class="tcp title">
		<a href="/dashboard/" onClick="Page.Go(this.href); return false;">Обзор</a> » <a href="/dashboard/settings" onClick="Page.Go(this.href); return false;">Настройки</a></h2>
		<div class="cp">
<br>
<div class="form_continer">
	<form class="form_reg" method="POST" action="/dashboard/settings">
		<h1 class="form_reg_title">Основные настройки</h1>
        <div class="form_reg_body">                              
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="save[sitename]" value="<?= $save['sitename'] ?>" id="sitename" class="form-control">
                    <label class="blmd-label" for="sitename">Название сайта</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="save[sitelink]" value="<?= $save['sitelink'] ?>" id="sitelink" class="form-control">
                    <label class="blmd-label" for="sitelink">Адрес сайта</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="save[tpldir]" value="<?= $save['tpldir'] ?>" id="tpldir" class="form-control">
                    <label class="blmd-label" for="tpldir">Директория шаблонов</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="save[version]" value="<?= $save['version'] ?>" id="version" class="form-control">
                    <label class="blmd-label" for="version">Версия системы</label>
                </div>
            </div>
        </div>
        <div class="form_reg_body text-center">
            <!-- <button type="button" class="btn btn-blmd ripple-effect btn-success btn-lg btn-block">Сохранить</button> -->
            <input type="submit" value="Сохранить" name="save_conf" class="btn btn-blmd ripple-effect btn-success btn-lg btn-block" style="margin-top:0px" />
        </div>
    </form>
</div>

<div class="form_continer">
    <form class="form_reg" method="POST" action="/dashboard/settings">
        <h1 class="form_reg_title">База данных</h1>
        <div class="form_reg_body">                              
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="savebd[dbhost]" value="<?= $save['dbhost'] ?>" id="dbhost" class="form-control">
                    <label class="blmd-label" for="dbhost">Сервер</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="savebd[dbname]" value="<?= $save['dbname'] ?>" id="dbname" class="form-control">
                    <label class="blmd-label" for="dbname">Имя базы данных</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="savebd[dbuser]" value="<?= $save['dbuser'] ?>" id="dbuser" class="form-control">
                    <label class="blmd-label" for="dbuser">Имя пользователя</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="password" name="savebd[dbpass]" value="<?= $save['dbpass'] ?>" id="dbpass" class="form-control">
                    <label class="blmd-label" for="dbpass">пароль</label>
                </div>
            </div>
            <div class="input-group blmd-form">
                <div class="blmd-line">
                    <input type="text" name="save[dbpref]" value="<?= $save['dbpref'] ?>" id="dbpref" class="form-control">
                    <label class="blmd-label" for="dbpref">Префикс</label>
                </div>
            </div>      
        </div>
        <div class="form_reg_body text-center">
            <input type="submit" value="Сохранить" name="save_bd" class="btn btn-blmd ripple-effect btn-success btn-lg btn-block" style="margin-top:0px" />
        </div>
    </form>
</div>

</div>		
</div>	
</div>