<?php
class ModelDashboard extends Model
{

    public function renderMenu($page, $language)
    {
		$sections = RenderMenu::cp($page, $language);
		return $sections;
	}

	public function logs()
	{
        $query = "SELECT * FROM ".DB_PRE."_logs ";
        $res = $this->DB->select($query, $params = false);
		$count = count($res);
		$logs='';
		for ($i=0; $i < $count; $i++) { 
			$logs .= 
'<section>
<h3 style="font-size: 18px;">'.$res[$i]['log_date'].'</h3>
<p>'.$res[$i]['log_query'].'</p>
</section>';
		}
		return $logs;
	}

public function clear_log()
{
$this->DB->table_delete($table=DB_PRE.'_logs');
$columns="
  `id` int(11) NOT NULL auto_increment,
  `log_date` varchar(255) NOT NULL default '',
  `log_user` varchar(255) NOT NULL default 'N/A',
  `log_query` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
";
$this->DB->table_create($table=DB_PRE.'_logs', $columns);
	return true;
}

	// public function menu()
	// {

	// }

    public function all_users()
    {
        $query = "SELECT * FROM ".DB_PRE."_users ";
        $all_users = $this->DB->select($query, $params = false);

		$count = count($all_users);
		$users_all ='';

		for ($a=0,$b=0,$i=1; $b < $count; $i++) { 
			$a+=1;
			if ($a==1) {
				$d='class="tb5n21"';
			} elseif ($a==2){
				$d=" ";
				$a=0;
			}

			$users_all .= '
			<div '. $d .'>'.$all_users[$b]['id'].'</div>
			<div '.$d.'>'.$all_users[$b]['username'].'</div>
			<div '.$d.'>'.$all_users[$b]['email'].'</div>
			<div '.$d.'>'.$all_users[$b]['user_group'].'</div>
			<div '.$d.'>Действие</div>';
			$b +=1;//код с 1 плагина
		}
        return $users_all;
    }
	public function news()
	{
		$method='appnews';
		$result = API::query($method, $params=array(), $url='livemon.ru/api/' );
		$count = ($result -> response[0] -> count);
		$news = '';

		for ($i=0; $i < $count; $i++) { 
			$id[$i] 	 = ($result -> response[0] -> news[$i] -> id);
			$title[$i] 	 = ($result -> response[0] -> news[$i] -> title);
			$body[$i] 	 = ($result -> response[0] -> news[$i] -> body);
			$news .= 
'<section>
<h3 style="font-size: 18px;">'.($result -> response[0] -> news[$i] -> title).'</h3>
<p>'.$body[$i].'</p>
</section>';
		}
		return $news;
	}

	public function settings_get()
	{
		$save = array(
			'sitename' => SITENAME,
			'sitelink' => SITELINK,
			'tpldir' => TPLDIR,
			'version' => VERSION,
			'dbhost' => DB_HOST, 
			'dbuser' => DB_USER, 
			'dbpass' => DB_PWD, 
			'dbname' => DB_NAME, 
			'dbpref' => DB_PRE, 

			);
		return $save;
	}

	public function settings_save_conf($saves)
	{
		// обновляем настройки
        $file=APP . '/config.php';

/*		$newfile = RD . '/application/config_old.php';
		if (!copy($file, $newfile)) {
			unlink($newfile);
			if (!copy($file, $newfile)) {
		    	echo "не удалось скопировать $newfile...\n";
			}
		}*/
        unlink($file);

        $file=APP . '/config.php';

        $sitename = $saves['sitename'];
        $sitelink = $saves['sitelink'];
        $tpldir = $saves['tpldir'];
        $version = $saves['version'];

        $dbhost = DB_HOST;
        $dbname = DB_NAME;
        $dbuser = DB_USER;
        $dbpass = DB_PWD;   
        $dbprefix = DB_PRE;   
		$home_title = HOME_TITLE;
		$description = DESCRIPTION;
		$keywords = KEYWORDS;
		$home_mod = HOME;
        //если файла нету... тогда
        if( !file_exists($file)) {
        $handler = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту), мы создаем файл

        fwrite($handler, "<?php \n\n//System Configurations\n\n\$CONFIG = array (\n\n");
		fwrite($handler, "'SITELINK' => \"$sitelink\",\n\n");
		fwrite($handler, "'SITENAME' => \"$sitename\",\n\n");
		fwrite($handler, "'HOME_TITLE' => \"$home_title\",\n\n");
		fwrite($handler, "'DESCRIPTION' => \"$description\",\n\n");
		fwrite($handler, "'KEYWORDS' => \"$keywords\",\n\n");
		fwrite($handler, "'HOME' => \"$home_mod\",\n\n");
		fwrite($handler, "'TPLDIR' => \"$tpldir\",\n\n");
		fwrite($handler, "'VERSION' => \"$version\",\n\n");
		fwrite($handler, "'DB_NAME' => \"$dbname\",\n\n");
		fwrite($handler, "'DB_USER' => \"$dbuser\",\n\n");
		fwrite($handler, "'DB_HOST' => \"$dbhost\",\n\n");
		fwrite($handler, "'DB_PWD' => \"$dbpass\",\n\n");
		fwrite($handler, "'DB_PRE' => \"$dbprefix\",\n\n");
        fwrite($handler, ");\n\n?>" );
        fclose ($handler);
        }

	}

	public function settings_save_bd($saves)
	{
		// обновляем настройки
        $file=APP . '/config.php';
        unlink($file);

        $file=APP . '/config.php';

        $sitename = SITENAME;
        $sitelink = SITELINK;
        $tpldir = TPLDIR;
        $version = VERSION;
		$home_title = HOME_TITLE;
		$description = DESCRIPTION;
		$keywords = KEYWORDS;
		$home_mod = HOME;
        $dbhost = $saves['dbhost'];
        $dbname = $saves['dbname'];
        $dbuser = $saves['dbuser'];
        $dbpass = $saves['dbpass'];   
        $dbpref = $saves['dbpref'];   

        //если файла нету... тогда
        if( !file_exists($file)) {
        $handler = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту), мы создаем файл

        fwrite($handler, "<?php \n\n//System Configurations\n\n\$CONFIG = array (\n\n");
        fwrite($handler, "'SITENAME' => \"$sitename\",\n\n");
        fwrite($handler, "'SITELINK' => \"$sitelink\",\n\n");
		fwrite($handler, "'HOME_TITLE' => \"$home_title\",\n\n");
		fwrite($handler, "'DESCRIPTION' => \"$description\",\n\n");
		fwrite($handler, "'KEYWORDS' => \"$keywords\",\n\n");
		fwrite($handler, "'HOME' => \"$home_mod\",\n\n");
        fwrite($handler, "'TPLDIR' => \"$tpldir\",\n\n");
        fwrite($handler, "'VERSION' => \"$version\",\n\n");
        fwrite($handler, "'DB_HOST' => \"$dbhost\",\n\n");
        fwrite($handler, "'DB_NAME' => \"$dbname\",\n\n");
        fwrite($handler, "'DB_USER' => \"$dbuser\",\n\n");
        fwrite($handler, "'DB_PWD' => \"$dbpass\",\n\n");
        fwrite($handler, "'DB_PRE' => \"$dbpref\",\n\n");
        fwrite($handler, ");\n\n?>" );
        fclose ($handler);
        }

	}	

	public function info()
	{

	    $mysqli = function_exists('mysqli_connect') ? 'Поддерживается' : 'Неподдерживается';
	    $zlib = extension_loaded('zlib') ? 'Поддерживается' : 'Неподдерживается';
	    $xml = extension_loaded('xml') ? 'Поддерживается' : 'Неподдерживается';
	    $json = extension_loaded('json') ? 'Поддерживается' : 'Неподдерживается';

		$result = API::query($method='index', $params=array() );
		$ver_server = ($result -> response[0] -> version);
		$ver = VERSION;
		if ($ver < $ver_server) {
			$ver_status = $ver.'<span style="color: red;"> Устарела</span> <a href="/update/index" style="color: red;" onClick="Page.Go(this.href); return false;">Обновить</a>';
		}elseif ($ver == $ver_server) {
			$ver_status = '<span style="color: green;">'.$ver.'</span>';
		}elseif ($ver > $ver_server) {
			$ver_status = $ver.'<span style="color: green;">DEV</span>';
		}

		$info = array(
			'v' => $ver_status, 
			'php'=> phpversion(),
			'mysqli' => $mysqli,
			'zlib' => $zlib,
			'xml' => $xml,
			'json' => $json,
			);
		return $info;
	}

	public function menu_up($value)
	{
        $query = "SELECT * FROM ".DB_PRE."_menu";
        $menu = $this->DB->select($query, $params = false);

		$v=$value+1; //=3>2
		$va=$v-1;

		$v1=$v-1; // =2>3
		$vb=$v1-1;
		// 3>2 2>3
		$cloud_2 =array();
		$cloud_3 =array();
		$cloud_2 = array(
		'title'    	  => $menu[$vb]['title'],
		'title_url'   => $menu[$vb]['title_url'],
		'parent'      => $menus[$vb]['parent'],
		);
		$cloud_3 = array(
		'title'   	  => $menu[$va]['title'],
		'title_url'   => $menu[$va]['title_url'],
		'parent'	  => $menu[$va]['parent'],
		);

        $where = "id = " . $v;
        $this->DB->update(DB_PRE.'_menu', $where, array(
		'title'    	  => $cloud_2['title'],
		'title_url'   => $cloud_2['title_url']/*,
		'parent'      => $cloud_2['parent']*/
		));
        $where = "id = " . $v1;
        $this->DB->update(DB_PRE.'_menu', $where, array(
		'title'    	  => $cloud_3['title'],
		'title_url'   => $cloud_3['title_url']/*,
		'parent'      => $cloud_3['parent']*/
		));
	}

	public function menu_down($value)//1
	{
        $query = "SELECT * FROM ".DB_PRE."_menu";
        $menu = $this->DB->select($query, $params = false);

		$v=$value+1; //=3>2 //1+1=2
		$va=$v-1; //1

		$v1=$v+1; // =4>3 //3
		$vb=$v1-1;//2
		// 3>4 4>3
		$cloud_4 =array();
		$cloud_3 =array();
		$cloud_4 = array(
		'title'    	  => $menu[$vb]['title'],
		'title_url'   => $menu[$vb]['title_url'],
		'parent'      => $menus[$vb]['parent'],
		);
		$cloud_3 = array(
		'title'   	  => $menu[$va]['title'],
		'title_url'   => $menu[$va]['title_url'],
		'parent'	  => $menu[$va]['parent'],
		);

        $where = "id = " . $v1;//3
        $this->DB->update(DB_PRE.'_menu', $where, array(
		'title'    	  => $cloud_3['title'],
		'title_url'   => $cloud_3['title_url']/*,
		'parent'      => $cloud_3['parent']*/
		));
        $where = "id = " . $v;//2
        $this->DB->update(DB_PRE.'_menu', $where, array(
		'title'    	  => $cloud_4['title'],
		'title_url'   => $cloud_4['title_url']/*,
		'parent'      => $cloud_2['parent']*/
		));
	}

	public function menu_del($id)
	{
        $where = "id=".$id;
        $this->DB->delete(DB_PRE.'_menu', $where);
		return true;
	}

	public function header($content)
	{
		if ($content=='index') {
			$header = array(
				'title' => 'Livemon CMS', 
				'description' => 'Livemon - cовременная система управления сайтом',
				'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
				);		
		} elseif($content=='') {
			$header = array(
				'title' => 'Livemon CMS', 
				'description' => 'Livemon - cовременная система управления сайтом',
				'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
				);	
		}
		return $header;
	}
}