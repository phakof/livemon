<?php
class ModelRegister extends Model
{
    function __construct() {
        parent::__construct();
    }

	public function header($content)
	{
		if ($content=='index') {
			$header = array(
				'title' => HOME_TITLE, 
				'description' => DESCRIPTION,
				'keywords' => KEYWORDS,
				);		
		} elseif($content=='') {
			$header = array(
				'title' => HOME_TITLE, 
				'description' => DESCRIPTION,
				'keywords' => KEYWORDS,
				);	
		}
		return $header;
	}
	public function register($newUser)
	{
        $newUser['password'] = md5($newUser['password']);

        $this->DB->insert(DB_PRE.'_users', array(
        'username' => $newUser['username'], 
        'email' => $newUser['email'], 
        'user_group' => 3, 
        'password' => $newUser['password']));
	return true;
	}

}