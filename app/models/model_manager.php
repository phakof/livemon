<?php
class ModelManager extends Model
{
    public function renderMenu($page, $language)
    {
		$sections = RenderMenu::cp($page, $language);
		return $sections;
	}

// Статус модулей
// 1 вкл
// 2 выкл
// 3 не установлен
// 4 не найден

    public function all_modules()
    {
        $query = "SELECT * FROM ".DB_PRE."_modules ";
        $all_modules = $this->DB->select($query, $params = false);
		$count = count($all_modules);
		$mods_all ='';
		for ($a=0,$b=0,$i=1; $b < $count; $i++) { 

			$a+=1;
			if ($a==1) {
				$d='class="tb5n21"';
			} elseif ($a==2){
				$d=" ";
				$a=0;
			}
	        if ($all_modules[$b]['status']=='1') {
	        	$all_modules[$b]['status']='вкл';
	        	$all_modules[$b]['status_title']='Удалить';
	        	$all_modules[$b]['status_install']='#';
	        	$all_modules[$b]['v_s']	= '';
				if ($all_modules[$b]['title'] == 'api' || $all_modules[$b]['title'] == 'dashboard' || $all_modules[$b]['title'] == 'main' || $all_modules[$b]['title'] == 'manager' || $all_modules[$b]['title'] == 'tools') {
					// $all_modules[$b]['version'] = VERSION;
					$all_modules[$b]['status_title']='#';
					$all_modules[$b]['status_install']='#title';
				}else{
					$method12='modules';	
					$result12 = API::query($method12, $params=array(), $url='livemon.ru/api/' );
					$count12 = ($result12 -> response[0] -> count);
					for ($qw12=0; $qw12 < $count12; $qw12++) { 
						$this_mod[$qw12]['title']	 = ($result12 -> response[0] -> items[$qw12] -> title);
						$this_mod[$qw12]['name']	 = ($result12 -> response[0] -> items[$qw12] -> name);
						$this_mod[$qw12]['body'] = ($result12 -> response[0] -> items[$qw12] -> body);
						$this_mod[$qw12]['version'] = ($result12 -> response[0] -> items[$qw12] -> version);
						$this_mod[$qw12]['url'] = ($result12 -> response[0] -> items[$qw12] -> url);
						if ($all_modules[$b]['name'] == $this_mod[$qw12]['name']) {
							if ($all_modules[$b]['version'] < $this_mod[$qw12]['version']) {
								$all_modules[$b]['v_s']	= '<a style="color:red" href="/update/Step1_mod/'.$all_modules[$b]['name'].'/'.$all_modules[$b]['version'].'" onClick="Page.Go(this.href); return false;">Обновить</a>';
							}elseif ($all_modules[$b]['version'] > $this_mod[$qw12]['version'] || $all_modules[$b]['version'] == $this_mod[$qw12]['version']) {
								$all_modules[$b]['v_s']	= '';
							}
						}
					}
					$all_modules[$b]['status_title']='Удалить';
					$all_modules[$b]['status_install']='/manager/delete_mod?name='.$all_modules[$b]['name'];
				}
	        }elseif ($all_modules[$b]['status']=='2') {
	        	$all_modules[$b]['status']='выкл';
	        	$all_modules[$b]['status_title']=' ';
	        	$all_modules[$b]['status_install']='#';
	        	$all_modules[$b]['v_s']	= '';
	        }elseif ($all_modules[$b]['status']=='3') {
	        	$all_modules[$b]['status']='не установлен';
	        	$all_modules[$b]['status_title']='Установить';
	        	$all_modules[$b]['status_install']='/manager/install_mod?name='.$all_modules[$b]['name'];
	        	$all_modules[$b]['v_s']	= '';
	        }elseif ($all_modules[$b]['status']=='4') {
	        	$all_modules[$b]['status']='не найден';
	        	$all_modules[$b]['status_title']=' ';
	        	$all_modules[$b]['status_install']='#';
	        	$all_modules[$b]['v_s']	= '';     	
	        }  
			$mods_all .= '<div '. $d .'>'.$i.'</div><div '.$d.'>'.$all_modules[$b]['version'].'</div><div '.$d.'>'.$all_modules[$b]['title'].'</div><div '.$d.'>'.$all_modules[$b]['body'].'</div><div '.$d.'>'.$all_modules[$b]['status'].' '.$all_modules[$b]['v_s'].'</div> <div '.$d.'><a href="'.$all_modules[$b]['status_install'].'">'.$all_modules[$b]['status_title'].'</a></div> ';
			$b +=1;//код с 1 плагина
		}
        return $mods_all;
    }


    public function addons($type='')
    {
    	if ($type=='mod') {
			$method='modules';			
    	}else if($type=='tpl'){
    		$method='templates';
    	}
		$result = API::query($method, $params=array(), $url='livemon.ru/api/' );

		$count = ($result -> response[0] -> count);
		$c2=$count+1; 
		$addons = array();
		if ($type=='mod') {
			for ($qw=0; $qw < $count; $qw++) { 
				$title 	 = ($result -> response[0] -> items[$qw] -> title);
				$name 	 = ($result -> response[0] -> items[$qw] -> name);
				$body 	 = ($result -> response[0] -> items[$qw] -> body);
				$version = ($result -> response[0] -> items[$qw] -> version);
				$url 	 = ($result -> response[0] -> items[$qw] -> url);

				$addons[] .= '
				<section class="mod">
				    <div class="text-j">
					    <h2 style="text-align: left;">'.$title.'</h2>
					    <p>'.$body.'</p>
					    <p>Версия '.$version.'</p>
					    <a href="'.SITELINK.'manager/download?url='. $url.'&name='.$name.'&addon=mod">Скачать</a>
				    </div>
			    </section>'; 
			}
		}elseif($type=='tpl'){
			for ($qw=0; $qw < $count; $qw++) { 
				$title 	 = ($result -> response[0] -> items[$qw] -> title);
				$name 	 = ($result -> response[0] -> items[$qw] -> name);
				$body 	 = ($result -> response[0] -> items[$qw] -> body);
				$version = ($result -> response[0] -> items[$qw] -> version);
				$url 	 = ($result -> response[0] -> items[$qw] -> url);

				$addons[] .= '
				<section class="mod">
				    <div class="text-j">
					    <h2 style="text-align: left;">'.$title.'</h2>
					    <p>'.$body.'</p>
					    <p>Версия '.$version.'</p>
					    <a href="'.SITELINK .'manager/download?url='. $url.'&name='.$name.'&addon=tpl">Скачать</a>
				    </div>
			    </section>'; 
			}
		}
		
		//$addons = $count;
        return $addons;
    }

    public function download($url, $name, $addon)
    {

		$path = RD . '/uploads/addons/'.$addon.'/'.$name.'.zip';

		//распаковка
		if ($addon == 'mod') {
			$todir = RD . '/modules/';
		}elseif ($addon == 'tpl') {
			$todir = RD . '/tpl/';
		}

		file_put_contents($path, file_get_contents($url));
		
		$zip = new ZipArchive;
		if ($zip->open($path) === TRUE) {
		    $zip->extractTo($todir);
		    $zip->close();
		unlink($path);
		    // удача
		} else {
		    // неудача
		}


		if ($addon !== 'mod' AND $addon !== 'tpl') {
			return false;
		}else{
			return true;
		}
    }

	public function modules_update()
	{
		// Обновляем список
		$path = MOD;
		$mods = scandir($path);
		$count = count($mods);

		for ($i=2; $i < $count; $i++) { 
	        $query = "SELECT * FROM ".DB_PRE."_modules WHERE `name`={?} ";
	        $params = array($mods[$i]);
	        $mod_bd = $this->DB->selectRow($query, $params); 

	        $mod_live = 'update';

			if ($mod_bd['name']==$mods[$i]) { // Если есть, то Обновляем
				$path = RD . '/modules/'.$mods[$i].'/';

				if( !file_exists($path . '/load.php')) {
		
			        $where = "name=".$mods[$i];
			        $this->DB->delete(DB_PRE.'_modules', $where);
			
				}else{
				include $path . '/load.php';

				// Находим id плагина, чтобы обновить в бд
				// ! можно проще
		        $query = "SELECT * FROM ".DB_PRE."_modules WHERE `name`={?} ";
		        $params = array($mods[$i]);
		        $mod_bd = $this->DB->selectRow($query, $params); 				

		        $where = "id = " . $mod_bd['id'];
		        $this->DB->update(DB_PRE.'_modules', $where, array(
					'name' => $MODULE_NAME, 
					'title' => $MODULE_TITLE, 
					'body' => $MODULE_DESCRIPTION, 
					'version' => $MODULE_VERSION, 
		        ));
 
        		unset($mod);unset($menu); //откидываем 
				}
			}else{// Если нет, то Добавляем !$mod_bd['name']==$mods[$i]

				$path = RD . '/modules/'.$mods[$i].'/';
				include $path . '/load.php';

	        	$this->DB->insert(DB_PRE.'_modules', array(
					'name' => $MODULE_NAME, 
					'title' => $MODULE_TITLE, 
					'body' => $MODULE_DESCRIPTION, 
					'version' => $MODULE_VERSION, 
					'status' => '3',  // Осталось установить
				));
				unset($mod);unset($menu); //откидываем 
			}
		}
		return true;
	}

// Статус
// 1 вкл
// 2 выкл
// 3 не установлен
// 4 не найден

	public function install_mod($mod_name)
	{
		// работаем с меню
		$mod_live = 'install';
		$path = RD . '/modules/'.$mod_name.'/';
		require_once $path . '/load.php'; // Подключаем данные с модуля (+ дополнения) 
// Находим id плагина, чтобы обновить в бд
// ! можно проще
        $query = "SELECT * FROM ".DB_PRE."_modules WHERE `name`={?} ";
        $params = array($mod_name);
        $mod_bd = $this->DB->selectRow($query, $params); 

	    $where = "id = " . $mod_bd['id'];
	    $this->DB->update(DB_PRE.'_modules', $where, array(
			//'id' => $mod_bd['id'],
			'name' => $MODULE_NAME, 
			'title' => $MODULE_TITLE, 
			'body' => $MODULE_DESCRIPTION, 
			'version' => $MODULE_VERSION, 
			'status' => '1', // Установлен
	    ));

		//unset($mod);unset($menu); //откидываем 
		return true;
	}

/*
*
* Удалить модуль
* 	Удаляем из меню, если есть
* 	Удаляем из БД
*	Удаляем папку
*
*/
	public function delete_mod($mod_name)
	{
		// работаем с меню
		$mod_live ='uninstall';
		$path = RD . '/modules/'.$mod_name.'/';
		require_once $path . '/load.php'; // данные (+ допольные действия) 

		// Удаляем папку рекурсивно
		$make = Tools::removeRecursive($path);

		// Находим id плагина, чтобы удалить в бд
		// ! можно проще
        $query = "SELECT * FROM ".DB_PRE."_modules WHERE `name`={?} ";
        $params = array($mod_name);
        $mod_bd = $this->DB->selectRow($query, $params); 

        $where = "id=".$mod_bd['id'];
        $this->DB->delete(DB_PRE.'_modules', $where);

		//unset($mod);unset($menu); //откидываем 
		return true;
	}

    public function all_templates()
    {
		// Обновляем список
		$path = APP . '/tpl/';
		$tpls = scandir($path);
		$count = count($tpls);
		//$tpls['0'] = $count;
		return $tpls;
    }

	public function install_tpl_loc($tpl='')
	{
		if ($tpl !== '') {
			$tpl_name = $tpl;
		}else{
			$tpl_name = TPLDIR;
		}
		// обновляем настройки
        $file=APP . '/config.php';
        unlink($file);

        $file=APP . '/config.php';

        $sitename = SITENAME;
        $sitelink = SITELINK;
        $tpldir = $tpl_name;
        $version = VERSION;

        $dbhost = DB_HOST;
        $dbname = DB_NAME;
        $dbuser = DB_USER;
        $dbpass = DB_PWD;   
        $dbprefix = DB_PRE; 
		$home_title = HOME_TITLE;
		$description = DESCRIPTION;
		$keywords = KEYWORDS;
		$home_mod = HOME;

        //если файла нету... тогда
        if( !file_exists($file)) {
        $handler = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту), мы создаем файл

        fwrite($handler, "<?php \n\n//System Configurations\n\n\$CONFIG = array (\n\n");
		fwrite($handler, "'SITELINK' => \"$sitelink\",\n\n");
		fwrite($handler, "'SITENAME' => \"$sitename\",\n\n");
		fwrite($handler, "'HOME_TITLE' => \"$home_title\",\n\n");
		fwrite($handler, "'DESCRIPTION' => \"$description\",\n\n");
		fwrite($handler, "'KEYWORDS' => \"$keywords\",\n\n");
		fwrite($handler, "'HOME' => \"$home_mod\",\n\n");
		fwrite($handler, "'TPLDIR' => \"$tpldir\",\n\n");
		fwrite($handler, "'VERSION' => \"$version\",\n\n");
		fwrite($handler, "'DB_NAME' => \"$dbname\",\n\n");
		fwrite($handler, "'DB_USER' => \"$dbuser\",\n\n");
		fwrite($handler, "'DB_HOST' => \"$dbhost\",\n\n");
		fwrite($handler, "'DB_PWD' => \"$db_pass\",\n\n");
		fwrite($handler, "'DB_PRE' => \"$dbprefix\",\n\n");
        fwrite($handler, ");\n\n?>" );
        fclose ($handler);
        }
		return true;
	}

	public function header($content)
	{
		if ($content=='index') {
			$header = array(
				'title' => 'Livemon CMS', 
				'description' => 'Livemon - cовременная система управления сайтом',
				'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
				);		
		} elseif($content=='') {
			$header = array(
				'title' => 'Livemon CMS', 
				'description' => 'Livemon - cовременная система управления сайтом',
				'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
				);	
		}
		return $header;
	}
} 