<?php
class ModelMain extends Model
{
    function __construct() {
        parent::__construct();
    }

	public function header($content)
	{
		if ($content=='index') {
			$header = array(
				'title' => HOME_TITLE, 
				'description' => DESCRIPTION,
				'keywords' => KEYWORDS,
				);		
		} elseif($content=='') {
			$header = array(
				'title' => HOME_TITLE, 
				'description' => DESCRIPTION,
				'keywords' => KEYWORDS,
				);	
		}
		return $header;
	}

}