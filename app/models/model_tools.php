<?php
class ModelTools extends Model
{

    function __construct() {
        parent::__construct();
    }

    public function header($content)
    {
        if ($content=='index') {
            $header = array(
                'title' => 'Livemon CMS', 
                'description' => 'Livemon - cовременная система управления сайтом',
                'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
                );      
        } elseif($content=='') {
            $header = array(
                'title' => 'Livemon CMS', 
                'description' => 'Livemon - cовременная система управления сайтом',
                'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
                );  
        }
        return $header;
    }

    public function access_token($token)
    {
		$method = 'access_token';
		$params=array(
		'client_id' => '1',
		'client_secret' => 'h9FaZy7C8fdBSe1v0GVi6JUkHbMgTAKXNPcml2nIWtQpr5wD4o',
		'token' => token,
		    );
		$url='livemon.ru/api/';

        $query = API::query($method, $params=array(), $url='');

        return $query;
    }

    public function login($userData)
    {   

        $query = 
            "SELECT * 
             FROM ".DB_PRE."_users 
             WHERE `username`={?} 
             AND `password`={?}";

        $params = array($userData['username'], md5($userData['password']));
        $user = $this->DB->selectRow($query, $params);

        if ($user) {
            //add to session
            $user = Users::getUserObj($user['id']);
            $_SESSION['usr']['obj'] = serialize($user);
            $_SESSION['usr']['id']  = $user->id;
            return $user;
        } else {
            $this->error = 'Данные не верны!';
            return false;
        }
    }
   
    public function lm_id($user=array())
    {
        $query = "SELECT * FROM ".DB_PRE."_users WHERE `lm_id`={?} ";
        $params = array($user['lm_id']);
        $user_tbl = $this->DB->selectRow($query, $params); 

        return $user_tbl['lm_id'];
    }    

}