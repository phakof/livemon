<?php
class ModelUpdate extends Model
{
	public function header($content)
	{
		if ($content=='index') {
			$header = array(
				'title' => 'Livemon CMS', 
				'description' => 'Livemon - cовременная система управления сайтом',
				'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
				);		
		} elseif($content=='') {
			$header = array(
				'title' => 'Livemon CMS', 
				'description' => 'Livemon - cовременная система управления сайтом',
				'keywords' => 'cms, движок для сайта, система управления сайтом, разработка сайта, система управления контентом, Content Management System, Livemon CMS',
				);	
		}
		return $header;
	}

//OLD
/*
	public function step($step0)
	{
        $filename = RD. '/uploads/update/step.txt';

        $text = file_get_contents($filename);
        // $step;
        $step = unserialize($text);
        
		if ($step0 == 1) {
		        if ($step==0 || $step=='') { // save
		            $step = 1;

		        }elseif($step==2){
		        header("Location: /update/step3");
		        }elseif($step==3){
		        header("Location: /update/step4");
		        }elseif($step==4){
		        header("Location: /update/step5");
		        }elseif($step==5){
		        header("Location: /update/finish");
		        }
		}elseif ($step0==2) {
		        if ($step==1) { // old step
		            $step = 2;
		        }
		}elseif ($step0==3) {
		        if ($step==2) { // old step
		            $step = 3;
		        }
		}elseif ($step0==4) {
		        if ($step==3) { // old step
		            $step = 4;
		        }
		}elseif ($step0==5) {
		        if ($step==4) { // old step
		            $step = 5;
		        }
		}elseif ($step0==6) {
		        if ($step==5) { // old step
		            $step = 6;
		        }
		}
       
        $text = serialize($step);
        file_put_contents($filename, $text);
        return true;
	}*/

	public function step1_mod($addon, $version)
	{
		$params=array();
		$params['addon']=$addon;
		$params['version']=$version;

		$result = API::query($method='update_mod', $params );
		//$version = ($result -> response[0] -> version);
		$path = ($result -> response[0] -> path);
        $path_file = RD . '/uploads/tmp/'.$addon.'.zip';
        file_put_contents( $path_file, file_get_contents($path)) ;
		return true;
	}	

	public function step2_mod($addon)
	{
        $path = RD . '/uploads/tmp/'.$addon.'.zip';
        $todir = RD . '/uploads/tmp/'.$addon.'/';

        $zip = new ZipArchive;
        if ($zip->open($path) === TRUE) {
            // путь к каталогу, в который будут помещены файлы
            $zip->extractTo($todir);
            $zip->close();
        unlink($path);
            // удача
	       	return true;     
        } else {
            // неудача
            return false;
        }
	}

	public function step3_mod($addon) // удаляем/копируем MOD
	{
        $path = RD . '/uploads/tmp/'.$addon.'/';
        require_once $path . 'delete.php'; // Подключаем step3.1
        require_once $path . 'copy.php'; // Подключаем step3.2
        return true;
	}

	public function step4_mod($addon) // шаг 4 # запросы в бд MOD
	{
        $path = RD . '/uploads/tmp/'.$addon.'/';
        require_once $path . 'db.php'; // Подключаем step4.1
        return true;
	}

	public function step5_mod($addon) // шаг 5 # Завершение MOD
	{
        $path = RD . '/uploads/tmp/'.$addon.'/';
        // Удаляем папку рекурсивно
        $make = Tools::removeRecursive($path);
        return true;
	}

//System

	public function step1()
	{

		$params=array();
		$params['host']=SITENAME;
		$params['version']=VERSION;

		$result = API::query($method='update', $params );
		//$version = ($result -> response[0] -> version);
		$path = ($result -> response[0] -> path);

        $path_file = RD . '/uploads/update.zip';

        file_put_contents( $path_file, file_get_contents($path)) ;

		return true;
	}	

	public function step2()
	{
        $path = RD . '/uploads/update.zip';
        $todir = RD . '/uploads/';

        $zip = new ZipArchive;
        if ($zip->open($path) === TRUE) {
            // путь к каталогу, в который будут помещены файлы
            $zip->extractTo($todir);
            $zip->close();
        unlink($path);
            // удача
	       	return true;     
        } else {
            // неудача
            return false;
        }
	}

	public function step3()
	{
        $path = RD . '/uploads/update/';
        require_once $path . 'delete.php'; // Подключаем step3.1
        require_once $path . 'copy.php'; // Подключаем step3.2
        return true;
	}

	public function step4()
	{
        $path = RD . '/uploads/update/';
        require_once $path . 'db.php'; // Подключаем step4.1
        return true;
	}

	public function step5()
	{
        $path = RD . '/uploads/update/';
        require_once $path . 'info.php';
        // $v;

        // обновляем настройки
        $file=APP . '/config.php';

/*		$newfile = RD . '/application/config_old.php';
		if (!copy($file, $newfile)) {
		    echo "не удалось скопировать $file...\n";
		}*/
        unlink($file);

        $file=APP . '/config.php';

        $sitename = SITENAME;
        $tpl = TPLDIR;
        $db_name = DB_NAME;
        $db_user = DB_USER;
        $db_host = DB_HOST;
        $db_pass = DB_PWD;   
        $dbprefix = DB_PRE;
        $sitelink = SITELINK;
		$home_title = HOME_TITLE;
		$description = DESCRIPTION;
		$keywords = KEYWORDS;
		$home_mod = HOME;
        //если файла нету... тогда
        if( !file_exists($file)) {
        $handler = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту), мы создаем файл

        fwrite($handler, "<?php \n\n//System Configurations\n\n\$CONFIG = array (\n\n");
		fwrite($handler, "'SITELINK' => \"$sitelink\",\n\n");
		fwrite($handler, "'SITENAME' => \"$sitename\",\n\n");
		fwrite($handler, "'HOME_TITLE' => \"$home_title\",\n\n");
		fwrite($handler, "'DESCRIPTION' => \"$description\",\n\n");
		fwrite($handler, "'KEYWORDS' => \"$keywords\",\n\n");
		fwrite($handler, "'HOME' => \"$home_mod\",\n\n");
		fwrite($handler, "'TPLDIR' => \"$tpl\",\n\n");
		fwrite($handler, "'VERSION' => \"$v\",\n\n");
		fwrite($handler, "'DB_NAME' => \"$db_name\",\n\n");
		fwrite($handler, "'DB_USER' => \"$db_user\",\n\n");
		fwrite($handler, "'DB_HOST' => \"$db_host\",\n\n");
		fwrite($handler, "'DB_PWD' => \"$db_pass\",\n\n");
		fwrite($handler, "'DB_PRE' => \"$dbprefix\",\n\n");
        fwrite($handler, ");\n\n?>" );
        fclose ($handler);
        }

        // Удаляем папку рекурсивно
        $path = RD . '/uploads/update/';
        $make = Tools::removeRecursive($path);
        return true;
	}
}