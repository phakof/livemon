<?php
class ModelApi extends Model
{

    function __construct() {
        parent::__construct();
        header('Content-Type', 'application/json');
    }

    public function main()
    {
        $time = time();
        $version ='0.9';
        $response = array (
            "response" => array ([
                "time" => $time,
                "version" => $version,
            ])
        );
        return Api::res_json($response);
    }

}