<?php  
class RenderMenu extends ActiveRecord
{
    function __construct()
    {

    }

    public function get_menu()
    {
/*        $filename = RD. '/application/cache/menu.tmp';
        $text = file_get_contents($filename);//прочитать из файла
        $menu2 = array();
        $menu2 = unserialize($text);//восстановить массив из текстового представления*/
        $DB = DataBase::getDB();
        $query = "SELECT * FROM ".DB_PRE."_menu";
        $menu2 = $DB->select($query, $params = false);

        return $menu2;
    }

    public function get_submenu($item)
    {
/*        $filename = RD. '/application/cache/menu_sub.tmp';
        $text = file_get_contents($filename);//прочитать из файла
        $menu2 = array();
        $menu2 = unserialize($text);//восстановить массив из текстового представления*/
        $DB = DataBase::getDB();
        $query = "SELECT * FROM ".DB_PRE."_menu";
        $menu2 = $DB->select($query, $params = false);

        $submenu = array();

        $count = count($menu2) -1;

        for ($i=0; $i < $count; $i++) {
            if ($menu2[$i]['parent'] == $item) {
                $submenu[] = array(
                    'title'    => $menu2[$i]['title'],
                    'title_url'    => $menu2[$i]['title_url'],
                    'parent'    => $menu2[$i]['parent'],
                );
            }else{}
        }
        return $submenu;
    }

    public function cp($page,$language){
            $menu = array('dashboard' => ['link'     => '/dashboard/',
                                     'class'    => 'news',
                                     'title'    => ['ru' => 'Обзор',
                                                    'eu' => 'Обзор',
                                                   ],
                                     'name'     => ['ru' => 'Обзор',
                                                    'eu' => 'Обзор',
                                                   ],
                                     'visible'  => true
                                    ],
/*
                          'manager' => ['link'  => '/manager/',
                                        'class' => 'tickets',
                                        'title' => ['ru' => 'Менеджер',
                                                    'eu' => 'Менеджер',
                                                    ],
                                        'name'  =>  ['ru' => 'Менеджер',
                                                    'eu' => 'Менеджер',
                                                    ],
                                        'visible'   => true
                                        ],
*/
                          'modules' => ['link'  => '/manager/modules',
                                        'class' => 'tickets',
                                        'title' => ['ru' => 'Модули',
                                                    'eu' => 'Модули',
                                                    ],
                                        'name'  =>  ['ru' => 'Модули',
                                                    'eu' => 'Модули',
                                                    ],
                                        'visible'   => true
                                        ],
                          'templates' => ['link'  => '/manager/templates',
                                        'class' => 'tickets',
                                        'title' => ['ru' => 'Шаблоны',
                                                    'eu' => 'Шаблоны',
                                                    ],
                                        'name'  =>  ['ru' => 'Шаблоны',
                                                    'eu' => 'Шаблоны',
                                                    ],
                                        'visible'   => true
                                        ],
                          'users' => ['link'    => '/dashboard/users',
                                     'class'    => 'cases',
                                     'title'    => ['ru' => 'Пользователи',
                                                    'eu' => 'Пользователи',
                                                   ],
                                     'name'     => ['ru' => 'Пользователи',
                                                    'eu' => 'Пользователи',
                                                   ],
                                     'visible'  => true
                                    ],
                           'system' => ['link'    => '/dashboard/system',
                                     'class'    => 'cases',
                                     'title'    => ['ru' => 'Система',
                                                    'eu' => 'Система',
                                                   ],
                                     'name'     => ['ru' => 'Система',
                                                    'eu' => 'Система',
                                                   ],
                                     'visible'  => true
                                    ],
/*                          'reports' => ['link'    => '/dashboard/reports',
                                     'class'    => 'cases',
                                     'title'    => ['ru' => 'Управление отчетами',
                                                    'eu' => 'Управление отчетами',
                                                   ],
                                     'name'     => ['ru' => 'Управление отчетами',
                                                    'eu' => 'Управление отчетами',
                                                   ],
                                     'visible'  => true
                                    ],
                          'statistics' => ['link'    => '/dashboard/statistics',
                                     'class'    => 'cases',
                                     'title'    => ['ru' => 'Статистика Сайта',
                                                    'eu' => 'Статистика Сайта',
                                                   ],
                                     'name'     => ['ru' => 'Статистика Сайта',
                                                    'eu' => 'Статистика Сайта',
                                                   ],
                                     'visible'  => true
                                    ],*/

                          'menu' => ['link'    => '/dashboard/menu',
                                     'class'    => 'cases',
                                     'title'    => ['ru' => 'Меню',
                                                    'eu' => 'Меню',
                                                   ],
                                     'name'     => ['ru' => 'Меню',
                                                    'eu' => 'Меню',
                                                   ],
                                     'visible'  => true
                                    ],                        


                           'settings' => ['link'    => '/dashboard/settings',
                                     'class'    => 'cases',
                                     'title'    => ['ru' => 'Настройки',
                                                    'eu' => 'Настройки',
                                                   ],
                                     'name'     => ['ru' => 'Настройки',
                                                    'eu' => 'Настройки',
                                                   ],
                                     'visible'  => true
                                    ],
                         );
            $sections = '';
            foreach ($menu as $key => $value) {
                if($value['visible'] === true){
                    if($key == $page){
                        $active = 'active';
                    }else{
                        $active = '';
                    }  
                    $sections .= '<li class="pl10"><a href="'.$value['link'].'" onClick="Page.Go(this.href); return false;" class="bar_linck '.$active.'">'.$value['name'][$language].'</a>
            </li>';
                }
            }
     
            return $sections;
        }
}