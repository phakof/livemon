<?php

class Users extends ActiveRecord
{
	public $id          = NULL;
	public $username    = NULL;
	public $password    = NULL;
    public $email       = NULL;
	public $name	    = NULL;	
	public $surname	    = NULL;	
	public $fullname	= NULL;	
    public $img         = NULL;
    public $log_time    = NULL;
	public $user_group  = NULL;
	public $info        = NULL;


    public function save($new = false, $primary = 'id')
    {
        if (parent::save($new, $primary)) {
            $_SESSION['usr']['upd'] = 1;
            return true;
        } else return false;
    }
    
	public static function getUserObj($value, $what = '*', $column = '`id`')
	{
	    $DB = DataBase::getDB();
        $query = 
        " SELECT ".$what.
        " FROM ".DB_PRE."_users ".
        " WHERE ".$column." = {?}";
        
        $params = array($value);
        
		$userData = $DB->selectRow($query, $params);
        
		if (!empty($userData)) {
			return new Users($userData);
		} else {
			return false;
		}
	}
 }