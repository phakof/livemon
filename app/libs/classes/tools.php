<?php
class Tools
{
    public static function getSiteData()
    {
        $DB = DataBase::getDB();
        $query = 
        " SELECT `id` ".
        " FROM ".DB_PRE."_users ";
        $site['total_users'] = count($DB->select($query));
        
        $query = 
        " SELECT `username` ".
        " FROM ".DB_PRE."_users ".
        " WHERE (`login_time` + 30*60) > ".time();
        
        $site['online_users'] = $DB->select($query);
        $site['online_count'] = count($site['online_users']);
        
       Registry::set('site', (object)$site);
    }
    
    public static function checkDatabase()
    {
        $mysqli = @mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_NAME);
        if (mysqli_connect_errno($mysqli))
            Registry::set('mysql_error', "Не удалось подключиться к MySQL: " . mysqli_connect_error());
        
        Registry::set('mysqli', $mysqli);
    }
    
    public static function getUserData()
    {
        $logged = false;
        if (!empty($_SESSION['usr']['obj']) && is_string($_SESSION['usr']['obj']))
            $bool = unserialize($_SESSION['usr']['obj']);
        if (isset($_SESSION['usr']['id']) && !empty($bool)) {
            if (!empty($_SESSION['usr']['upd'])) {
                $user = Users::getObj($_SESSION['usr']['id']);
                $_SESSION['usr']['obj'] = serialize($user);
                $_SESSION['usr']['upd'] = 0;
            } else {
                $user = unserialize($_SESSION['usr']['obj']);
            }
            Registry::set('user', $user);
            $logged = true;
        }
        define('LOGGED', $logged);
    }
    
    public static function getSiteConfig()//Настройки сайта
    {
        require APP . '/settings.php';
    }
    
    public static function getAccessData()
    {
        // G - Гость
        // L - Пользователь
        // O - Модератор
        // A - Администратор
        $G = true;
        $L = $O = $A = false;
        
        if (LOGGED) {
            $L = true;
            $G = false;
            $user = Registry::get('user');
            if ($user->user_group == '2') {
                $O = true;
            }
            if ($user->user_group == '1') {
                $A = true;
            }
        }
        define('G', $G); define('L', $L); define('O', $O); define('A', $A);
    }
        
         
    public static function sendMail($from = false, $for, $topic, $message)
    {
        if (!$from) $from = SITENAME;
            
        $headers      = "Content-type: text/html; charset=utf-8 \r\nFrom: ".$from;;
        $date         = "'".date("Y-m-d H:i:s",time())."'";  
        $fool_message = "Вам написал: <font color=grey>".$from."</font><hr />".
                        "Дата: <font color=grey>".$date."</font><hr />".
                        "Тема обращения: <font color=grey>".$topic."</font><hr />".
                        "<h2><center>Сообщение:</center></h2><hr /> ".$message;
        if (mail($for, $topic, $fool_message, $headers)) {
            return true;
        }
    }

    public static function removeRecursive($path)
    {
        if ($objs = glob($path."/*")) {
           foreach($objs as $obj) {
             is_dir($obj) ? Tools::removeRecursive($obj) : unlink($obj);
           }
        }
        rmdir($path);     
    }

/*    public static function setLog($ip)
    {
        if (LOGGED) { // если пользователь
            $id = Registry::get('user')->id;

        $query = "SELECT * FROM `stats` WHERE `user`={?} ";
        $params = array($id);
        $user = $this->DB->selectRow($query, $params);
        
        if ($user==true) { // если есть 
            $count=$user['count']+1;
            $last_day = substr($user['date'], -2);
            if ($last_day == date(d)) { # обновляем
                $where = "id = " . $post_id;
                $this->DB->update('blog', $where, array(
                    //'id' => $post_id,
                    'title' => $new_post[title], 
                    'body' => $new_post[body], 
                    'full_body' => $new_post[full_body], 
                    'category' => $new_post[category], 
                    'date' => $new_post[date], 
                    'img' => $new_post[img]
                ));
            }elseif($last_day < date(d)){ # добавляем
                $this->DB->insert('stats', array(
                    //'id' => '4',
                    'ip' => $ip, 
                    'user' => $id, 
                    'date' => date(Y-m-d), #0000-00-00 ггггммдд
                    'count' => 1
                );
            }
        }else{ # если гость

        $query = "SELECT * FROM `stats` WHERE `user`={?} ";
        $params = array(0);
        $user = $this->DB->selectRow($query, $params);

            if ($last_day == date(d)) { # обновляем
                
            }elseif($last_day < date(d)){ # добавляем
                $this->DB->insert('stats', array(
                    //'id' => '4',
                    'ip' => $ip, 
                    'user' => '0', 
                    'date' => date(Y-m-d), #0000-00-00 ггггммдд
                    'count' => 1
                );
            }else{
            $user = 'guest';
            }
        }

    }*/
}