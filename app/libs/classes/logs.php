<?php  
class Logs
{
    function __construct()
    {

    }

    public function go($query)
    {

        $log = $query;
        // $log = 'Вход на главную';
        $user_id = Registry::get('user')->id;

        $date_log = date('Y-m-d');
        // $date_log = '111';
        $new = array(
            'date' => $date_log, 
            'user' => $user_id, 
            'query' => $log, 
        );
        $DB = DataBase::getDB();
        $DB->insert(DB_PRE.'_logs', array(
            'log_date' => $new['date'], 
            'log_user' => $new['user'], 
            'log_query' => $new['query'], 
        ));           
       return true;
    }
}