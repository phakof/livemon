<?php
@ob_start ();
@ob_implicit_flush ( 0 );

session_start();

	/*@ini_set ( 'display_errors', true );
	@ini_set ( 'html_errors', false );
	if( !defined( 'E_DEPRECATED' ) ) {
	    @error_reporting ( E_ALL ^ E_WARNING ^ E_NOTICE );
	    @ini_set ( 'error_reporting', E_ALL ^ E_WARNING ^ E_NOTICE );
	} else {
	    @error_reporting ( E_ALL ^ E_WARNING ^ E_DEPRECATED ^ E_NOTICE );
	    @ini_set ( 'error_reporting', E_ALL ^ E_WARNING ^ E_DEPRECATED ^ E_NOTICE );
	}*/

header("Content-Type: text/html; charset=utf-8");
if ( file_exists(RD . '/install.php') AND !file_exists(APP . '/config.php') ) {
    header( "Location: ".str_replace("index.php","install.php",$_SERVER['PHP_SELF']) );
    die ( "Livemon CMS not installed. Please run install.php" );
}

//Определяем, что к нам идёт Ajax запрос
@$to_ajax = ($_GET['ajax'] == 1) ? 1 : 0 ;
$http_ajax = (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ? true : false ;
$dajax = ($to_ajax == 1 || $http_ajax == true) ? true : false ;

define('AJAX', $dajax);

require_once APP . '/core/autoload.php';
require_once APP . '/core/registry.php';
require_once APP . '/core/model.php';
require_once APP . '/core/view.php';
require_once APP . '/core/controller.php';
require_once APP . '/core/router.php';

Tools::getSiteConfig();
//Основные переменные
/*
$secret_key = '123456789'; //   Секретный ключ
$activation_key = KEY; //   Индивидуальный ключ активации
$rest = substr(SITELINK, 7, -1); 
$rest = str_replace("/", "", $rest);
$domain = $rest; //Текущий домен
//  Создаем строку с данными через разделитель "|"
$data = $activation_key."|".$domain;
//Генерируем защитный md5 хэш из всех трех переменных
$hash = md5($secret_key.$data);
//  Создаем строку для отправки отделяя хэш палочкой "|"
$str = $hash."|".$data;
//  И переводим строку в base64, чтобы не возникало проблем при отправке
$final_str = ($str);
$method='lic';
//  $params=array();
$params['lic']=$str;
$params['right_domain']=$domain;
$result = API::query($method, $params, $url='livemon.ru/api/');
// echo ($result -> response[0] -> ok);
$path = ($result -> response[0] -> ok);
if ($path!==true) {
    die("Неизвестная ошибка. Попробуйте переустановить скрипт.");
}
*/
//----------------------------
Tools::checkDatabase();
//----------------------------
Tools::getSiteData(); # Если вы хотите
//----------------------------
Tools::getUserData();
//----------------------------
Tools::getAccessData();

// Tools::setLog($_server['REMOTE_ADDR']); # Статистика

Router::start();